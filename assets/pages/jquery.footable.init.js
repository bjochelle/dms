/**
 * Theme: Metrica - Responsive Bootstrap 4 Admin Dashboard
 * Author: Mannatthemes
 * Footable Js
 */

$(function () {
	"use strict";
	
	/*Init FooTable*/
	$('#footable-1,#footable-2').footable();
	
	/*Editing FooTable*/
	var $modal = $('#editor-modal'),
	$editor = $('#editor'),
	$editorTitle = $('#editor-title'),
	ft = FooTable.init('#footable-3', {
		editing: {
			enabled: true,
			addRow: function(){
				$modal.removeData('row');
				$editor[0].reset();
				$editorTitle.text('Add a new row');
				$modal.modal('show');
			},
			editRow: function(row){
				var values = row.val();
				$editor.find('#dealerID').val(values.dealerID);
				$editor.find('#firstName').val(values.firstName);
				$editor.find('#lastName').val(values.lastName);
				$editor.find('#address').val(values.address);
				$editor.find('#conNum').val(values.conNum);
				$editor.find('#emailAdd').val(values.emailAdd);

				$modal.data('row', row);
				$editorTitle.text('Edit row #' + values.dealerID);
				$modal.modal('show');
			},
			deleteRow: function(row){
				swal.fire({
				        title: 'Are you sure?',
				        text: "You won't be able to revert this!",
				        type: 'warning',
				        showCancelButton: true,
				        confirmButtonText: 'Yes, delete it!',
				        cancelButtonText: 'No, cancel!',
				        reverseButtons: true
				      }).then((result) => {
				        if (result.value) {
				            var values = row.val();
							$editor.find('#dealerID').val(values.dealerID);
							var dealer = values.dealerID;
							var action = 'delete';
							$.post("../ajax/CRUD_dealer.php", {
								dealer: dealer,
								action: action
							}, function(data){
								if(data == 1){
									alert_notif("All Good!","Data was Successfully deleted.","success");
								}else{
									alert_notif("Aw Snap!","Unable to finish transaction, Please Try again.","danger");
								}
							});
				        } else if (
				          // Read more about handling dismissals
				          result.dismiss === Swal.DismissReason.cancel
				        )
				         {
				          swal.fire(
				            'Cancelled',
				            'Your imaginary file is safe :)',
				            'error'
				          )
				        }
				    });
			}
		}
	}),
	uid = 10;
$editor.on('submit', function(e){
	if (this.checkValidity && !this.checkValidity()) return;
	e.preventDefault();
	$("#saveDealer").prop("disabled", true);
	$("#saveDealer").html("<span class='fa fa-spin fa-spinner'></span> Loading");
	var row = $modal.data('row'),
		values = {
			id: $editor.find('#dealerID').val(),
			firstName: $editor.find('#firstName').val(),
			lastName: $editor.find('#lastName').val(),
			address: $editor.find('#address').val(),
			conNum: $editor.find('#conNum').val(),
			emailAdd: $editor.find('#emailAdd').val(),
			company_id: $editor.find('#companyID').val()
		};
	
	if (row instanceof FooTable.Row){
		var dID = values.id;
		var fname = values.firstName;
		var lname = values.lastName;
		var address = values.address;
		var conNum = values.conNum;
		var emailAdd = values.emailAdd;
		var action = "update";
		$.post("../ajax/CRUD_dealer.php", {
			dID: dID,
			fname: fname,
			lname: lname,
			address: address,
			conNum: conNum,
			emailAdd: emailAdd,
			action: action
		}, function(data){
			if(data == 1){
				alert_notif("All Good!","Data was Successfully Updated.","success");
			}else{
				alert_notif("Aw Snap!","Unable to finish transaction, Please Try again.","danger");
			}
		});

	} else {
		//alert(values.company_id);
		var fname = values.firstName;
		var lname = values.lastName;
		var address = values.address;
		var conNum = values.conNum;
		var emailAdd = values.emailAdd;
		var action = "add";
		var company_id = values.company_id;

		$.post("../ajax/CRUD_dealer.php", {
			fname: fname,
			lname: lname,
			address: address,
			conNum: conNum,
			emailAdd: emailAdd,
			action: action,
			company_id: company_id
		}, function(data){
			if(data == 1){
				alert_notif("All Good!","Data was Successfully added.","success");
			}else{
				alert_notif("Aw Snap!","Unable to finish transaction, Please Try again.","danger");
			}
		});
	};
	$modal.modal('hide');
	// window.location.reload();
});
});