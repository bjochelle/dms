/**
 * Theme: Metrica - Responsive Bootstrap 4 Admin Dashboard
 * Author: Mannatthemes
 * Footable Js
 */

$(function () {
	"use strict";
	
	/*Init FooTable*/
	$('#footable-1,#footable-2').footable();
	
	/*Editing FooTable*/
	var $modal = $('#editor-modal'),
	$editor = $('#editor'),
	$editorTitle = $('#editor-title'),
	ft = FooTable.init('#footable-3', {
		editing: {
			enabled: true,
			addRow: function(){
				$modal.removeData('row');
				$editor[0].reset();
				$editorTitle.text('Add a new row');
				$modal.modal('show');
			},
			editRow: function(row){
				var values = row.val();

				$editor.find('#supply_id').val(values.supply_id);
				
				// $editor.find('#product_id').val(values.product_id);
				// $editor.find('#company_id').val(values.company_id);
				$editor.find('#product_id option:selected').text(values.product_id);

				$editor.find('#company_id option:selected').text(values.company_id);
				$editor.find('#qty').val(values.qty);
				$editor.find('#date_added').val(values.date_added);
				$modal.data('row', row);
				$editorTitle.text('Edit row #' + values.supply_id);
				$modal.modal('show');
			},
			deleteRow: function(row){
				swal.fire({
				        title: 'Are you sure?',
				        text: "You won't be able to revert this!",
				        type: 'warning',
				        showCancelButton: true,
				        confirmButtonText: 'Yes, delete it!',
				        cancelButtonText: 'No, cancel!',
				        reverseButtons: true
				      }).then((result) => {
				        if (result.value) {
				            var values = row.val();
							$editor.find('#supply_id').val(values.supply_id);
							var sID = values.supply_id;
							var action = 'delete';
							$.post("../ajax/CRUD_supply.php", {
								sID: sID,
								action: action
							}, function(data){
								// alert(data);
								if(data == 1){
									alert_notif("All Good!","Data was Successfully deleted.","success");
								}else{
									alert_notif("Aw Snap!","Unable to finish transaction, Please Try again.","danger");
								}
							});
				        } else if (
				          // Read more about handling dismissals
				          result.dismiss === Swal.DismissReason.cancel
				        )
				         {
				          swal.fire(
				            'Cancelled',
				            'Your imaginary file is safe :)',
				            'error'
				          )
				        }
				    });
			}
		}
	}),
	uid = 10;
$editor.on('submit', function(e){
	if (this.checkValidity && !this.checkValidity()) return;
	e.preventDefault();
	$("#saveSupply").prop("disabled", true);
	$("#saveSupply").html("<span class='fa fa-spin fa-spinner'></span> Loading");
	var row = $modal.data('row'),
		values = {
			id: $editor.find('#supply_id').val(),
			product_id: $('#product_id option:selected').text(),
			company_id: $('#company_id option:selected').text(),
			qty: $editor.find('#qty').val(),
			date_added: $editor.find('#date_added').val()
		};
	
	if (row instanceof FooTable.Row){

		var sID = values.id;
		var product_id = values.product_id;
		var company_id = values.company_id;
		var qty = values.qty;
		var date_added = values.date_added;
		var action = "update";
		$.post("../ajax/CRUD_supply.php", {
			sID: sID,
			product_id: product_id,
			company_id: company_id,
			qty: qty,
			date_added: date_added,
			action: action
		}, function(data){
			if(data == 1){
				alert_notif("All Good!","Data was Successfully Updated.","success");
			}else{
				alert_notif("Aw Snap!","Unable to update Supply, Please Try again.","danger");
			}
		});

	} else {
		var product_id = values.product_id;
		var company_id = values.company_id;
		var qty = values.qty;
		var date_added = values.date_added;
		var action = "add";
		$.post("../ajax/CRUD_supply.php", {
			product_id: product_id,
			company_id: company_id,
			qty: qty,
			date_added: date_added,
			action: action,
		}, function(data){
			if(data == 1){
				alert_notif("All Good!","Data was Successfully added.","success");
			}else{
				alert_notif("Aw Snap!","Unable to add Supply, Please Try again.","danger");
			}
		});
	};
	$modal.modal('hide');
	// window.location.reload();
});
});