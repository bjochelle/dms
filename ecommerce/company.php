<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">CRM</a></li>
                        <li class="breadcrumb-item active">Branch Manager</li>
                    </ol>
                </div>
                <h4 class="page-title">Branch Manager</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
    <div class="row">
        <div class="col-lg-12">
         <a class="btn btn-primary px-4 btn-rounded float-right mt-0 mb-3  waves-effect waves-light" href="index.php?page=addCompany">+ Add New Branch Manager</a>
        </div>
        <div class="col-lg-12">

            <div class="accordion" id="Customers_collapse">
                <?php 

                $fetch = mysql_query("SELECT * FROM tbl_company  ORDER BY `tbl_company`.`company_name` ASC");
                $count=0;
                    while($row = mysql_fetch_array($fetch)){?>
                <div class="card">
                    <div class="card-body">
                        <a class="d-lg-flex justify-content-between" data-toggle="collapse" href="#Customer<?php echo $count;?>" role="button" aria-expanded="true" aria-controls="Customer<?php echo $count;?>">                                                
                            <div class="media mb-3 mb-lg-0">
                                <img src="../assets/images/<?php echo $row['company_image']?>" class="mr-3 thumb-md align-self-center rounded-circle" alt="...">
                                <div class="media-body align-self-center"> 
                                    <h5 class="mt-0 mb-1"> 
                                        <?php echo ucwords($row['company_name']);?>
                                    </h5>
                                    <p class="text-muted mb-0"><i class="fas fa-map-marker-alt mr-2 text-info"></i> <?php echo ucwords($row['company_address']);?> </p>                                                                                           
                                </div><!--end media body-->
                            </div> <!--end media-->
                            <p class="text-muted mb-2 mb-lg-0 align-self-center"><i class="far fa-envelope mr-2 text-info font-14"></i><?php echo ucwords($row['company_email']);?></p> 
                            <p class="text-muted mb-2 mb-lg-0 align-self-center"><i class="fas fa-phone mr-2 text-info font-14"></i><?php echo ucwords($row['company_no']);?></p>
                             <p class="text-muted mb-2 mb-lg-0 align-self-center"><button class="btn btn-success  btn-sm waves-effect waves-light" onclick="update(<?= $row['company_id']?>)"><i class="mdi mdi-pencil mr-1"></i> Update</button>'</p>                                           
                        </a>
                        <div class="collapse" id="Customer<?php echo $count;?>" data-parent="#Customers_collapse">
                            <div class="card card-body mb-0">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered mb-0 table-centered table-sm">
                                                <thead>
                                                <tr>
                                                    <th>Dealer Name</th>
                                                    <th>Address</th>
                                                    <th>Contact Number</th>
                                                    <th>Email</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $fetch_dealer = mysql_query("SELECT * FROM tbl_dealer where company_id='$row[company_id]'");
                                                    while($row_dealer = mysql_fetch_array($fetch_dealer)){?>
                                                <tr>
                                                    <td><?php echo ucwords(getFullname($row_dealer['user_id']))?></td>
                                                    <td><?php echo ucwords(getData($id,'tbl_user','address','user_id'));?></td>
                                                    <td><?php echo ucwords(getData($id,'tbl_user','contact_number','user_id'));?></td>
                                                    <td><?php echo ucwords(getData($id,'tbl_user','email','user_id'));?></td>
                                                </tr>
                                            <?php } ?>
                                                </tbody>
                                            </table><!--end /table-->
                                        </div><!--end /tableresponsive--> 
                                    </div><!-- end col-->
                                    <!-- end col-->
                                </div><!--end row-->                                                           
                            </div><!--end card-->
                        </div><!--end collapse-->                                   
                    </div><!--end card-body-->
                </div>
                <?php $count++;} ?>

            </div><!--end Customers_collapse-->
        </div><!--end col-->
    </div><!--end row-->

</div><!-- container -->
<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaCRM").addClass("active");
    $(".MetricaCRM_list").addClass("active");
    $("#link_company").addClass("active");
  });


   function update(id){
    window.location.replace("index.php?page=update-Company&id="+id);
  }
  </script>