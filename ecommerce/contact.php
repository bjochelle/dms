<?php 

    if($status == 'A'){?>
    <style type="text/css">
        /* .footable-editing{
            display: none;
        }  */
    </style>

<?php }?>

<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">EDMS</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">CRM</a></li>
                        <li class="breadcrumb-item active">DEALERS</li>
                    </ol>
                </div>
                <h4 class="page-title">Dealers</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0">Dealers List</h4>
                    <table id="footable-3" class="table mb-0" data-paging="true" data-filtering="true" data-sorting="true">
                        <thead>
                            <tr>
                                <th data-name="dealerID" data-breakpoints="xs" data-type="number">ID</th>
                                <th data-name="firstName">First Name</th>
                                <th data-name="lastName">Last Name</th>
                                <th data-name="address" data-breakpoints="xs">Address</th>
                                <th data-name="conNum" data-breakpoints="xs">Contact No.</th>
                                <th data-name="emailAdd" data-breakpoints="xs">Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $data = GETALLDEALERS($status,$company_id);
                            if(count($data) > 0){
                                foreach($data as $data_val){
                                    echo "<tr>";
                                        echo "<td>".$data_val['dealer_id']."</td>";
                                        echo "<td>".$data_val['fname']."</td>";
                                        echo "<td>".$data_val['lname']."</td>";
                                        echo "<td>".$data_val['address']."</td>";
                                        echo "<td>".$data_val['conNum']."</td>";
                                        echo "<td>".$data_val['emailAdd']."</td>";
                                    echo "</tr>";
                                }
                            }
                            ?>
                        </tbody>
                    </table><!--end table-->

                    <!--Editor-->
                    <div class="modal fade" id="editor-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title">
                    
                        <div class="modal-dialog" role="document">
                            <form class="modal-content form-horizontal" id="editor">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="editor-title">Add Rasdasdasow</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>                                                            
                                </div>
                                <div class="modal-body">
                                    <div class="form-group row" id="ids" style='display:none'>
                                        <label for="dealerID" class="col-sm-3 control-label">ID</label>
                                        <div class="col-sm-9">
                                            <input type="text" readonly class="form-control" id="dealerID" name="dealerID" placeholder="dealerID">
                                        </div>
                                    </div>
                                    <div class="form-group row" id="ids" style='display:none'>
                                        <label for="companyID" class="col-sm-3 control-label">ID</label>
                                        <div class="col-sm-9">
                                            <input type="text" readonly class="form-control" id="companyID" name="companyID" value='<?php echo $company_id; ?>' placeholder="companyID">
                                        </div>
                                    </div>
                                    <div class="form-group required row">
                                        <label for="firstName" class="col-sm-3 control-label">First Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" required>
                                        </div>
                                    </div>
                                    <div class="form-group require row">
                                        <label for="lastName" class="col-sm-3 control-label">Last Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="address" class="col-sm-3 control-label">Address</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="address" name="address" placeholder="Address">
                                        </div>
                                    </div>
                                    <div class="form-group required row">
                                        <label for="conNum" class="col-sm-3 control-label">Contact No.</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="conNum" name="conNum" placeholder="Contact Number" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="emailAdd" class="col-sm-3 control-label">Email Address</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="emailAdd" name="emailAdd" placeholder="Email Address">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" id='saveDealer' class="btn btn-primary">Save changes</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div><!--end modal-->
                </div><!--end card-body-->
            </div><!--end card-->
        </div><!--end col-->
    </div><!--end row-->

</div><!-- container -->
   <script src="../assets/pages/jquery.footable.init.js"></script> 
<script type="text/javascript">
  $(document).ready(function(){
    //dealerTable();
    $(".MetricaCRM").addClass("active");
    $(".MetricaCRM_list").addClass("active");
    $("#link_contact").addClass("active");
    // $("#editor").on('submit', function(e){
    //     e.preventDefault();
    //     alert("Test");
    // });

  });
  function dealerTable(){
    $("#footable-3").DataTable().destroy();
    $("#footable-3").dataTable({
        "searching": false
    });

    $(".footable-editing").hide(); 
  }
  </script>