<?php 
$id = $_REQUEST['id'];
?>
<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item"><a href="index.php?page=company-products">Products</a></li>
                        <li class="breadcrumb-item active"> Add Products</li>
                    </ol>
                </div>
                <h4 class="page-title">Products</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
    
    <div class="row">  
        <div class='col-12'>
            <div class="card">
                <div class='card-body'>
                    <?php if($id == 0){ ?>
                       <form onsubmit="return false" method='POST' id='product-add' enctype="multipart/form-data">
                        <div class='col-12'>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label text-right">Product Image</label>
                                <div class="col-sm-10">
                                    <input type="file" id="input-file-now-custom-1" name="product_image" class="dropify" data-default-file="../assets/images/users/user-4.jpg"/>
                                </div>
                            </div>
                        </div>
                        <div class='col-12'>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label text-right">Product Demo</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="file"  placeholder="URL link"/>
                                    <!-- <input type="file" id="input-file-now-custom-1" name="file" class="dropify" /> -->
                                </div>
                            </div>
                        </div>
                        <div class='col-12'>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label text-right">Product Name</label>
                                <div class="col-sm-10">
                                    <input type="text" id="product_name" name="product_name" placeholder='Product Name' class="form-control"/>
                                    <input type="hidden" id="p_action" name="p_action" placeholder='' value='add' class="form-control"/>
                                    <input type="hidden" id="company_id" name="company_id" placeholder='' value='<?php echo $company_id; ?>' class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class='col-12'>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label text-right">Product Description</label>
                                <div class="col-sm-10">
                                    <textarea rows='5' id='prod_desc' name='prod_desc' style='resize: none;' class='form-control' placeholder='Description'></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='col-12'>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label text-right">Price</label>
                                <div class="col-sm-10">
                                    <input type="number" id="product_price" name="product_price" step="0.01" placeholder='Product Price' class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class='col-12'>
                            <div class="form-group row">
                                <button type='submit' id='btn-add' class='btn btn-sm btn-primary pull-right'><span class='fa fa-check-circle'></span> Save Changes</button>
                            </div>
                        </div>
                    </form>

                    <?php }else { 
                        $details = mysql_fetch_array(mysql_query("SELECT * FROM tbl_product WHERE product_id = '$id'"));
                    ?>
                    <form method='POST' id='product-update' onsubmit="return false" enctype="multipart/form-data">
                        <div class='col-12'>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label text-right">Product Image</label>
                                <div class="col-sm-10">
                                    <input type="file" id="input-file-now-custom-1" name="update_product_image" class="dropify" data-default-file="../assets/images/products/<?php echo $details['filename']?>"/>
                                </div>
                            </div>
                        </div>
                           <div class='col-12'>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label text-right">Product Demo</label>
                                <!-- <div class="col-sm-10">
                                    <input type="file" id="input-file-now-custom-1" name="file" class="dropify" data-default-file="../assets/images/product_demo/<?php echo $details['video']?>"/>
                                </div> -->
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="file" value="<?php echo $details['video']?>" placeholder="URL link"/>
                                </div>
                            </div>
                        </div>
                        <div class='col-12'>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label text-right">Product Name</label>
                                <div class="col-sm-10">
                                    <input type="text" id="update_product_name" name="update_product_name" value="<?php echo $details['product_name']?>" class="form-control"/>
                                    <input type="hidden" id="p_action" name="p_action" placeholder='' value='update' class="form-control"/>
                                    <input type="hidden" id="update_company_id" name="update_company_id" placeholder='' value='<?php echo $company_id; ?>' class="form-control"/>
                                    <input type="hidden" id="productID" name="productID" value="<?php echo $id; ?>"/>
                                </div>
                            </div>
                        </div>

                        <div class='col-12'>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label text-right">Product Description</label>
                                <div class="col-sm-10">
                                    <textarea rows='5' id='update_prod_desc' name='update_prod_desc' style='resize: none;' class='form-control'><?php echo $details['description']?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='col-12'>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-sm-2 col-form-label text-right">Price</label>
                                <div class="col-sm-10">
                                    <input type="number" id="update_product_price" step="0.01" name="update_product_price" value="<?php echo $details['price']
                                    ?>" class="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class='col-12'>
                            <div class="form-group row">
                                <button type='submit' id='btn-update' class='btn btn-sm btn-primary pull-right'><span class='fa fa-check-circle'></span> Save Changes</button>
                            </div>
                        </div>
                    </form>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div><!--end row-->
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaCRM").addClass("active");
    $(".MetricaCRM_list").addClass("active");
    $("#link_company_products").addClass("active");

    $("#product-add").on('submit', function(e){
        e.preventDefault();
        $("#btn-add").prop("disabled", true);
        $("#btn-add").html("<span class='fa fa-spin fa-spinner'></span> Loading");
        $.ajax({
         url:"../ajax/CRUD_product.php",
         type: "POST",
         data:  new FormData(this),
         beforeSend: function(){},
         contentType: false,
         cache: false,
         processData:false,
         timeout: 600000,
         success: function(data)
            {
                alert_notif("All Good!","Product was successfully added.","success");
                // alert(data);
                // if(data == 1){
                    
                // }else if(data == 0){
                //     alert_notif("Aw Snap!","There's an error while saving the data, Please try again.","error");
                // }else{
                //     alert_notif("Aw Snap!","There's an error while saving the data, Please try again.","error");
                // }
                
            },
             error: function() 
            {
                 alert_notif("Aw Snap!","Unable to add new Company, Please Try again.","danger");
            }        
       });
    });
    $("#product-update").on('submit', function(e){
        e.preventDefault();
        $("#btn-update").prop("disabled", true);
        $("#btn-update").html("<span class='fa fa-spin fa-spinner'></span> Loading");
        $.ajax({
         url:"../ajax/CRUD_product.php",
         type: "POST",
         data:  new FormData(this),
         beforeSend: function(){},
         contentType: false,
         cache: false,
         processData:false,
         timeout: 600000,
         success: function(data)

            {
                    // alert(data)
                alert_notif("All Good!","Product was successfully updated.","success");
               
            },error: function() 
            {
                  alert_notif("Aw Snap!","Unable to add new Company, Please Try again.","danger");
            }


       });
    });
  });
  
  function productdetails(id){
        window.location.replace("index.php?page=productDetail&id="+id);
  }
</script>