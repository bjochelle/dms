<style type="text/css">
    /* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Ecommerce</a></li>
                        <li class="breadcrumb-item active">Cart</li>
                    </ol>
                </div>
                <h4 class="page-title">Cart</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0">Shopping Cart</h4>
                    <div class="table-responsive shopping-cart">
                        <table class="table mb-0" id="trans_list">
                            <thead>
                                <tr>
                                    <th style="width: 30%">Product</th>
                                    <th style="width: 15%">Price</th>
                                    <th style="width: 30%">Quantity</th>                        
                                    <th style="width: 15%">Total</th>
                                    <th style="width: 10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 

                                $fetch = mysql_query("SELECT * FROM tbl_transaction where user_id='$id' and status='S' and cart_status = 0 ORDER BY `tbl_transaction`.`date_added` DESC");
                                $subtotal = 0;
                                while($row = mysql_fetch_array($fetch)){
                                    $total = $row['qty']*getData($row['product_id'],'tbl_product','price','product_id');
                                     $filename = getData($row['product_id'],'tbl_product','filename','product_id');
                                    $subtotal+=$total;
                                ?>
                                <tr>
                                    <td>
                                        <img src="../assets/images/products/<?php echo $filename;?>" alt="" height="52">
                                        <p class="d-inline-block align-middle mb-0">
                                            <a href="index.php?page=productDetail&id=<?php echo $row['product_id'];?>" class="d-inline-block align-middle mb-0 product-name"><?php echo substr(getData($row['product_id'],'tbl_product','product_name','product_id'),0,20)?></a> 
                                            <!-- <br>
                                            <span class="text-muted font-13">size-08 (Model 2019)</span>  -->
                                        </p>
                                    </td>
                                    <td id="price<?php echo $row['trans_id'];?>"><?php echo number_format(getData($row['product_id'],'tbl_product','price','product_id'), 2)?></td>
                                    <td>
                                        <input class="form-control w-25" type="number" value="<?php echo $row['qty'];?>"  id="qty<?php echo $row['trans_id'];?>" onkeyup="calc(<?php echo $row['trans_id'];?>)">
                                    </td>
                                    <td id="total<?php echo $row['trans_id'];?>" class="price"><?php echo number_format($total, 2);?> </td>
                                    <td>
                                        <a href="#" class="text-dark" onclick="ConfirmDelete(<?php echo $row['trans_id'];?>)"><i class="mdi mdi-close-circle-outline font-18"></i></a>
                                    </td>
                                </tr>
                                <?php }
                                $final_total = $subtotal + 100;
                                ?>                                      
                            </tbody>
                        </table>
                    </div>
                    <div class="row justify-content-center">
                        
                        <div class="col-md-12">
                            <div class="total-payment">
                                <h4 class="header-title">Total Payment</h4>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td class="payment-title">Subtotal</td>
                                            <td>&#8369; <span id="subtotal"><?php echo number_format($subtotal, 2);?></span></td>
                                        </tr>
                                       <!--  <tr>
                                            <td class="payment-title">Shipping</td>
                                            <td>
                                                <ul class="list-unstyled mb-0">
                                                    <li>
                                                        <div class="radio radio-info">
                                                            <input type="radio" name="radio" id="radio_1" value="option_1" checked onkeyup="calc(0)">
                                                            <label for="radio_1">
                                                                Shipping Charge : &#8369; 100.00
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="radio radio-info">
                                                            <input type="radio" name="radio" id="radio_2" value="option_2" onkeyup="calc(0)">
                                                            <label for="radio_2">
                                                                Express Shipping Charge : &#8369; 150.00
                                                            </label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr> -->
                                        <tr>
                                            <td class="payment-title">Total</td>
                                            <td class="text-dark"><strong>&#8369; <span id="final_total"> <?php echo number_format($final_total, 2);?> </span></strong></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                            <div class="col-md-12 align-self-center">
                           
                            <div class="mt-4">
                                <div class="row">
                                    <div class="col-6">
                                        <a href="index.php?page=products" class="text-info"><i class="fas fa-long-arrow-alt-left mr-1"></i> Continue Shopping</a>
                                    </div>                                                        
                                    <div class="col-6 text-right">
                                        <a href="#" class="text-info" onclick="checkout()">Checkout <i class="fas fa-long-arrow-alt-right ml-1"></i></a>
                                    </div> 
                                </div>
                            </div>
                        </div><!--end col--> 
                        </div><!--end col--> 
                    </div><!--end row--> 
                </div><!--end card-->
            </div><!--end card-body-->
        </div><!--end col-->
    </div><!--end row--> 

</div><!-- container -->
<script type="text/javascript">
  $(document).ready(function(){
     $(".MetricaEcommerce").addClass("active");
    $(".MetricaEcommerce_list").addClass("active");
    $("#link_cart").addClass("active");

   
    // $("input[type='radio']").click(function(){
    //     if ($("#radio_1").prop("checked")) {
    //         var shipping_fee = 100;
    //     }else if($("#radio_2").prop("checked")) {
    //         var shipping_fee = 150;
    //     }

         var subtotal = $("#subtotal").text();
      //  $("#final_total").html(parseFloat(subtotal)+parseFloat(shipping_fee));

        // $("#final_total").html(parseFloat(subtotal));
    // });
  });

  function checkout(){
    // if ($("#radio_1").prop("checked")) {
        var ship = 0;
    // }else if($("#radio_2").prop("checked")) {
    //     var ship = 1;
    // }

     $.ajax({
        url:"../ajax/checkout.php",
        method:"POST",
        data:{ship:ship},
        success: function(data){
            window.location.replace("index.php?page=checkout&id="+data);
        }
    });

  
  }

  function ConfirmDelete(id){
     deleteData(id);
  }

  function dl(id){
     $.ajax({
        url:"../ajax/deleteTrans.php",
        method:"POST",
        data:{id:id},
        success: function(data){
            if(data == 1){
                success_delete();
                location.reload();
                // setTimeout(function(){
                //     $("#trans_list").load("index.php?page=cart #trans_list" );
                // },1000);

            }else{
                failedQuery();
            }
        }
    });
  }

  function calc(id){
        setTimeout(function(){
            
            var qty = $("#qty"+id).val();
            var price = $("#price"+id).text().replace(/\,/g,'');
            var total = (qty*price).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            $("#total"+id).text(total);

            var sum=0;
                $('#trans_list tbody tr').each(function() {
                    var  value = $(this).find(".price").html(); 
                    if(value==''){
                        var perTd = 0;
                    }else{
                        var perTd = parseFloat(value.replace(/\,/g,''));
                    }
                    sum += perTd;
                });

                $("#subtotal").html(sum.toFixed(2));

                // if ($("#radio_1").prop("checked")) {
                //     var shipping_fee = 100;
                // }else if($("#radio_2").prop("checked")) {
                //     var shipping_fee = 150;
                // }
                $("#final_total").html(sum.toFixed(2));

                $.ajax({
                    url:"../ajax/updateQty.php",
                    method:"POST",
                    data:{id:id,
                        qty:qty
                    },
                    success: function(data){
                       
                    }
                });
        },500)
  }
</script>