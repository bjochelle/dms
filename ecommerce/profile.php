<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
                <h4 class="page-title">Profile</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body  met-pro-bg">
                    <div class="met-profile">
                        <div class="row">
                            <div class="col-lg-4 align-self-center mb-3 mb-lg-0">
                                <div class="met-profile-main">
                                    <div class="met-profile-main-pic">
                                        <img src="../assets/images/user_image/<?php echo ucwords(getData($id,'tbl_user','filename','user_id'));?>" alt="" style="width: 128px;height: 128px;object-fit: cover;" class="rounded-circle">
                                        <span class="fro-profile_main-pic-change" data-toggle="modal" data-animation="bounce" data-target=".bs-example-modal-lg">
                                            <i class="fas fa-camera"></i>
                                        </span>
                                    </div>
                                    <div class="met-profile_user-detail">
                                        <h5 class="met-user-name"><?php echo ucwords(getFullname($id));?></h5>
                                    </div>
                                </div>                                                
                            </div><!--end col-->
                            <div class="col-lg-4 ml-auto">
                                <ul class="list-unstyled personal-detail">
                                    <li class=""><i class="dripicons-phone mr-2 text-info font-18"></i> <b> phone </b> : <?php echo ucwords(getData($id,'tbl_user','contact_number','user_id'));?></li>
                                    <li class="mt-2"><i class="dripicons-mail text-info font-18 mt-2 mr-2"></i> <b> Email </b> : <?php echo ucwords(getData($id,'tbl_user','email','user_id'));?></li>
                                    <li class="mt-2"><i class="dripicons-location text-info font-18 mt-2 mr-2"></i> <b>Location</b> : <?php echo ucwords(getData($id,'tbl_user','address','user_id'));?></li>
                                </ul>
                                <div class="button-list btn-social-icon">                                                
                                    <button type="button" class="btn btn-blue btn-round">
                                        <i class="fab fa-facebook-f"></i>
                                    </button>

                                    <button type="button" class="btn btn-secondary btn-round ml-2">
                                        <i class="fab fa-twitter"></i>
                                    </button>

                                    <button type="button" class="btn btn-pink btn-round  ml-2">
                                        <i class="fab fa-dribbble"></i>
                                    </button>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div><!--end f_profile-->                                                                                
                </div><!--end card-body-->
                <div class="card-body">
                    <ul class="nav nav-pills mb-0" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="general_detail_tab" data-toggle="pill" href="#general_detail">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="settings_detail_tab" data-toggle="pill" href="#settings_detail">Settings</a>
                        </li>
                    </ul>        
                </div><!--end card-body-->
            </div><!--end card-->
        </div><!--end col-->
    </div><!--end row-->
    <div class="row">
        <div class="col-12">
            <div class="tab-content detail-list" id="pills-tabContent">
                <div class="tab-pane fade show active" id="general_detail">
                    <div class="row">
                        <div class="col-12">                                            
                            <div class="card">
                                  <div class="card-body">
                                    <div class="">
                                        <form class="form-horizontal form-material mb-0" id="profile">
                                            <div class="form-group row">
                                                <input required type="hidden" name="user_id" class="form-control" value="<?php echo $id;?>">
                                                <div class="col-md-6">
                                                     <input required type="text" placeholder="First Name" name="fname" class="form-control" value="<?php echo ucwords(getData($id,'tbl_user','fname','user_id'));?>">
                                                </div>
                                                <div class="col-md-6">
                                                     <input required type="text" placeholder="Last Name" name="lname" class="form-control" value="<?php echo ucwords(getData($id,'tbl_user','lname','user_id'));?>">
                                                </div>
                                               
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <input required type="email" placeholder="Email" class="form-control" name="email" id="example-email" value="<?php echo getData($id,'tbl_user','email','user_id');?>">
                                                </div>
                                                <div class="col-md-4">
                                                     <input required type="text" placeholder="Phone No" class="form-control" name="contact_number"  value="<?php echo ucwords(getData($id,'tbl_user','contact_number','user_id'));?>">
                                                </div>
                                                <div class="col-md-4">
                                                    <input required type="date" placeholder="Birthday" class="form-control" name="bday"  value="<?php echo ucwords(getData($id,'tbl_user','bday','user_id'));?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <textarea rows="2" name="address"  placeholder="Address" class="form-control"><?php echo ucwords(getData($id,'tbl_user','address','user_id'));?></textarea>
                                                <button type="submit" class="btn btn-primary btn-sm text-light px-4 mt-3 float-right mb-0">Update Profile</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>    
                            </div><!--end card-->
                        </div><!--end col-->
                    </div><!--end row-->                                             
                </div><!--end general detail-->

                <div class="tab-pane fade" id="settings_detail">
                    <div class="row">
                        <div class="col-lg-12 mx-auto">
                            <div class="card">
                                <div class="card-body">
                                    <div class="">
                                        <form class="form-horizontal form-material mb-0" id="change_authentication">
                                            <div class="form-group row">
                                                 <input required type="hidden" name="user_id" class="form-control" value="<?php echo $id;?>">
                                                <div class="col-md-4">
                                                    <input required type="text" placeholder="Username" class="form-control" name="un"  value="<?php echo getData($id,'tbl_user','un','user_id');?>">
                                                </div>
                                                <div class="col-md-4">
                                                    <input required type="password" placeholder="Current Password" class="form-control"  name="curr_pw" id="curr_pw">
                                                </div>
                                                <div class="col-md-4">
                                                    <input required type="password" placeholder="New Password" class="form-control"  name="new_pw" id="new_pw">
                                                </div>
                                            </div>
                                          
                                            <div class="form-group">
                                                <button class="btn btn-primary btn-sm text-light px-4 mt-3 float-right mb-0">Change Authentication</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>                                            
                            </div>
                        </div> <!--end col-->                                          
                    </div><!--end row-->
                </div><!--end settings detail-->

                <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add New Opportunities</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <form id="change_image" method="post">
                                <div class="card">
                                <div class="card-body">
                                    <input required type="hidden" name="user_id" class="form-control" value="<?php echo $id;?>">
                                    <input type="file" id="file" name="filename" class="dropify" data-height="500" />
                                        </div><!--end card-body-->
                                    </div><!--end card-->
                                    <button type="submit" class="btn btn-sm btn-primary">Upload</button> 
                                              
                                </form>  
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->   


       
                </div><!--end settings detail-->
            </div><!--end tab-content--> 
            
        </div><!--end col-->
    </div><!--end row-->

</div><!-- container -->
<script type="text/javascript">
  $(document).ready(function(){
    $("body").attr("class","enlarge-menu");
  });

   $("#profile").submit(function(e){
    e.preventDefault();
     $.ajax({
        url:"../ajax/update_profile.php",
        method:"POST",
        data:$(this).serialize(),
        success: function(data){
        if(data == 1){
             alert_notif("Done","Successfully Updated Profile","success");
              setTimeout(function(){
               location.reload();
            },1000);
             
         }else if(data == 2){
            alert_notif("Sorry","Name already existing","warning");
        }else{
            failedQuery();
        }
        }
      });
    });

   $("#change_authentication").submit(function(e){
    e.preventDefault();

         $.ajax({
            url:"../ajax/update_password.php",
            method:"POST",
            data:$(this).serialize(),
            success: function(data){
            if(data == 1){
                 alert_notif("Done","Successfully Updated Authentication","success");
                  setTimeout(function(){
                   location.reload();
                },1000);
                 
             }else if(data == 2){
                alert_notif("Sorry","Username already existing","warning");
            }else if(data == 3){
                alert_notif("Sorry","Wrong Current Password","warning");
            }else{
                failedQuery();
            }
            }
          });
    });

   $("#change_image").submit(function(e){
    e.preventDefault();

     $.ajax({
        url:"../ajax/upload_image.php",
        method:"POST",
        data:new FormData(this),
        contentType:false,          // The content type used when sending data to the server.
        cache:false,                // To unable request pages to be cached
        processData:false,          // To send DOMDocument or non processed data file it is set to false
        timeout: 600000,
        success: function(data){

        if(data == 1){
             alert_notif("Done","Successfully Updated Profile Picture","success");
              setTimeout(function(){
               location.reload();
            },1000);
             
         }else if(data == 0){
            failedQuery();
           
        }else{
             alert_notif("Error!"," "+data+" ","warning");
        }

        
        }
      });
    });

  function updateImage(){

    }
</script>