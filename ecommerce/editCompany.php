 <?php 
$id = $_GET['id'];

$fetch_company = mysql_fetch_array(mysql_query("SELECT * FROM tbl_company where company_id='$id'"));
 ?>
 <div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item"><a href="index.php?page=company">Company</a></li>
                        <li class="breadcrumb-item active">Add Company</li>
                    </ol>
                </div>
                <h4 class="page-title">Profile</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body  met-pro-bg">
                    <div class="met-profile">
                        <div class="row">
                            <div class="col-lg-4 align-self-center mb-3 mb-lg-0">
                                <div class="met-profile-main">
                                    <div class="met-profile-main-pic">
                                        <img src="../assets/images/<?= $fetch_company['company_image']?>" alt="" class="rounded-circle" style="width: 100%;height: 130px;object-fit: cover;">
                                        <span class="fro-profile_main-pic-change">
                                            <i class="fas fa-camera"></i>
                                        </span>
                                    </div>
                                    <div class="met-profile_user-detail">
                                        <h5 class="met-user-name"><?= $fetch_company['company_name']?></h5>
                                    </div>
                                </div>                                                
                            </div><!--end col-->
                            <div class="col-lg-4 ml-auto">
                                <ul class="list-unstyled personal-detail">
                                    <li class=""><i class="dripicons-phone mr-2 text-info font-18"></i> <b> phone </b> : <?= $fetch_company['company_no']?></li>
                                    <li class="mt-2"><i class="dripicons-mail text-info font-18 mt-2 mr-2"></i> <b> Email </b> : <?= $fetch_company['company_email']?></li>
                                    <li class="mt-2"><i class="dripicons-location text-info font-18 mt-2 mr-2"></i> <b>Location</b> : <?= $fetch_company['company_address']?></li>
                                </ul>
                                <div class="button-list btn-social-icon">                                                
                                    <button type="button" class="btn btn-blue btn-round">
                                        <i class="fab fa-facebook-f"></i>
                                    </button>

                                    <button type="button" class="btn btn-secondary btn-round ml-2">
                                        <i class="fab fa-twitter"></i>
                                    </button>

                                    <button type="button" class="btn btn-pink btn-round  ml-2">
                                        <i class="fab fa-dribbble"></i>
                                    </button>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div><!--end f_profile-->                                                                                
                </div><!--end card-body-->
                <div class="card-body">
                    <ul class="nav nav-pills mb-0" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="general_detail_tab" data-toggle="pill" href="#general_detail">Branch Information</a>
                        </li>
                    </ul>        
                </div><!--end card-body-->
            </div><!--end card-->
        </div><!--end col-->
    </div><!--end row-->
    <form method="POST" id="company_details">
    <div class="row">
        <div class="col-12">
            <div class="tab-content detail-list" id="pills-tabContent">
                <div class="tab-pane fade show active" id="general_detail">
                    <div class="row">
                        <div class="col-12">                        
                            <div class="card">
                                 <input type="hidden" name='company_id' id='company_id' placeholder="Name" class="form-control" value="<?= $fetch_company['company_id']?>">
                                <div class="card-body">
                                     <span>Upload Branch Image</span>
                                    <input type="file" id="input-file-now-custom-1" name="company_profile" class="dropify" data-default-file="../assets/images/users/user-4.jpg"/>
                                    <br>
                                    <div class="">
                                        <div class="form-horizontal form-material mb-0">
                                            <div class="form-group">
                                                <input type="text" name='company_name' id='company_name' placeholder="Name" class="form-control" value="<?= $fetch_company['company_name']?>">
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <input type="email" placeholder="Email" class="form-control" name="company_email" value="<?= $fetch_company['company_email']?>" id="company_email" required>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="Contact Number" name="company_no" class="form-control" value="<?= $fetch_company['company_no']?>" id='company_no'>
                                                </div>
                                               
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <textarea rows="5" name='company_address' id='company_address' placeholder="Address" class="form-control"><?= $fetch_company['company_address']?></textarea>
                                                </div>
                                            </div>
                                           
                                            <div class="form-group">
                                               
                                                 <button type='submit' id='btn-save-company' class="btn btn-primary btn-sm text-light px-4 mt-3 float-right mb-0">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                            
                            </div>   
                        </div><!--end col-->
                    </div><!--end row-->                                             
                </div><!--end general detail-->

            

        
            
        </div><!--end col-->
    </div><!--end row-->
    </form>
</div><!-- container -->
<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaCRM").addClass("active");
    $(".MetricaCRM_list").addClass("active");
    $("#link_company").addClass("active");

    $("#company_details").on('submit', function(e){
        e.preventDefault();
        $("#btn-save-company").prop("disabled", true);
        $("#btn-save-company").html("<span class='fa fa-spin fa-spinner'></span> Loading");
        $.ajax({
         url:"../ajax/edit_company.php",
         type: "POST",
         data:  new FormData(this),
         beforeSend: function(){},
         contentType: false,
         cache: false,
         processData:false,
         success: function(data)
            {
                alert_notif("All Good!","Data was Successfully updated Company Information","success");
                 $("#btn-save-company").prop("disabled", false);
                 $("#btn-save-company").html("Save");  
            },
             error: function() 
            {
                alert_notif("Aw Snap!","Unable to add new Company, Please Try again.","danger");
                
            }  
                  
       });
    });
  });
  </script>