<?php 

$id = $_GET['id'];
$br_id = $_GET['br_id'];


function getStarValue($val){
    $id = $_GET['id'];
    if($val == 0){
     $count = mysql_fetch_array(mysql_query("SELECT avg(star_value) FROM tbl_review where product_id='$id' "));
    }else if($val==-1){
     $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_review where product_id='$id' "));
    }else{
    $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_review where product_id='$id' and star_value='$val'"));
    }
    return $count[0];
}

function getStarValuePerc($val){
    $id = $_GET['id'];

    $count_all = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_review where product_id='$id' "));
 
    $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_review where product_id='$id' and star_value='$val'"));

    if($count_all[0]==0){
        $rating = 0;
    }else{
        $rating = ($count[0]/$count_all[0])*100;
    }

    return $rating;
}
?>
<style type="text/css">
    .text-none{
        color: #ccc !important;
    }
</style>
<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Ecommerce</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Product</a></li>
                        <li class="breadcrumb-item active">Product-Detail</li>
                    </ol>
                </div>
                <h4 class="page-title">Product Detail</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <img src="../assets/images/products/<?php echo getData($id,'tbl_product','filename','product_id');?>" alt="" class=" mx-auto  d-block" height="400" style="width: 100%;">                                           
                        </div><!--end col-->
                        <div class="col-lg-6 align-self-center">
                            <form id="addCart">
                            <div class="single-pro-detail">
                                <h3 class="pro-title"><?php echo getData($id,'tbl_product','product_name','product_id')?></h3>
                                <input type="hidden" name="product_id" value="<?php echo $id?>">
                                <ul class="list-inline mb-2 product-review">
                                     <?php 
                                        $start=1;
                                        $rating = number_format(getStarValue(0),1);
                                        while($start<=5){
                                            if($start<=$rating){
                                                $style='text-warning';
                                                $diff = 0 ;
                                                $half = "";
                                            }else{
                                                $diff = abs($rating - $start);
                                                $style='text-none';
                                                if($diff<1 && $diff>0){
                                                    $style='text-warning';
                                                    $half = "-half";
                                                }else{
                                                    $half = "";
                                                    $style='text-none';
                                                }
                                            }
                                        ?>
                                        <li class="list-inline-item"><i class="mdi mdi-star<?php echo $half;?> <?php echo $style;?>"></i></li>
                                        <?php $start++;}
                                        ?>
                                    <li class="list-inline-item"><?php echo number_format(getStarValue(0),1);?> (<?php echo getStarValue(-1);?> reviews)</li>
                                </ul>
                                <h2 class="pro-price">&#8369; <?php echo number_format(getData($id,'tbl_product','price','product_id'),2)?></h2>                                                 
                                <h6 class="text-muted font-13">Description :</h6> 
                                <ul class="list-unstyled pro-features border-0" style="height: 200px;overflow: auto;">
                                    <li><?php echo getData($id,'tbl_product','description','product_id')?></li>
                                </ul>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <h3><a onclick="viewDemo()" style="cursor: help;">View Demo</a></h3>
                                    </div>
                                </div> 

                              
                                <?php if($status == 'C'){ ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="modal-status-select" class="mr-2">Dealer</label>
                                        <select class="custom-select select2 dealer_id" id="modal-status-select" name="dealer_id">
                                            <option selected="" value="">Select</option>
                                            <?php 

                                            if($br_id!='-1'){
                                                $fetch_dealer = mysql_query("SELECT * FROM `tbl_dealer` where company_id='$br_id'");
                                                while($row_dealer = mysql_fetch_array($fetch_dealer)){
                                                echo '<option value="'.$row_dealer['user_id'].'">'.getFullname($row_dealer['user_id']).' ( Branch :: '.getData($row_dealer['company_id'],'tbl_company','company_name','company_id').' )</option>';
                                                }

                                            }else{
                                                 $fetch_dealer = mysql_query("SELECT * FROM `tbl_dealer` ");
                                                while($row_dealer = mysql_fetch_array($fetch_dealer)){
                                                    $company_id = $row_dealer['company_id'];
                                                    
                                                    $sd = date('Y-m-d');

                                                    $count_supply = mysql_fetch_array(mysql_query("SELECT sum(qty) FROM tbl_supply where product_id='$id' and DATE_FORMAT(date_added, '%Y-%m-%d') <=  '$sd' and company='$company_id'"));

                                                    $count_trans = mysql_fetch_array(mysql_query("SELECT sum(qty) FROM tbl_transaction where product_id='$id' and status='F' and DATE_FORMAT(date_finish, '%Y-%m-%d') <=  '$sd' and company_id='$company_id'"))or die (mysql_error());


                                                     $diff = $count_supply[0] - $count_trans[0];

                                                     if($diff>0){
                                                         echo '<option value="'.$row_dealer['user_id'].'">'.getFullname($row_dealer['user_id']).' ( Branch :: '.getData($row_dealer['company_id'],'tbl_company','company_name','company_id').' )</option>';
                                                     }
                                               
                                                }
                                            }
                                           

                                            ?>
                                        </select>
                                    </div>
                                </div> 

                                <div class="quantity mt-3 ">
                                    <input class="form-control" type="number" min="1" value="1" id="example-number-input" required="" name="qty">
                                    <button type="submit" class="btn btn-primary text-white px-4 d-inline-block" onclick="//addQty(<?php echo $id;?>)"><i class="mdi mdi-cart mr-2"></i>Add to Cart</a>
                                </div> 
                                <?php } ?>                                         
                            </div>
                            </form>

                        </div><!--end col-->                                            
                    </div><!--end row-->
                </div><!--end card-body-->
            </div><!--end card-->
        </div><!--end col-->
    </div><!--end row-->

        <!-- modal -->
    <div class="modal about-modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">     
                        <h4 class="modal-title">Demo</h4>
                    </div> 
                    <div class="modal-body" id="modal_body">
                      
                    <?php
                    if(getData($id,'tbl_product','video','product_id') == ''){

                    }else{?>
                         <iframe width="100%" height="315" src="https://www.youtube.com/embed/<?php echo getData($id,'tbl_product','video','product_id');?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

                    <?php }?>

                    ?>

                    
                     
                    </div>
                </div>
            </div>
        </div>
    </div>

     <?php if($status == 'C'){ ?>
    <div class="row">            
        <div class="col-md-9">
             <div class="card">
              
                    <?php 
                        $fetch = mysql_query("SELECT * FROM tbl_review where product_id='$id' ORDER BY `tbl_review`.`date_added` DESC");
                       while($row = mysql_fetch_array($fetch)){?>
                          <div class="card-body">
                        <div class="row">
                        <div class="col-sm-9">
                            <div class="media">
                                <?php if(getData($row['user_id'],'tbl_user','filename',$row['user_id']) == ''){?>
                                    <img class="d-flex align-self-center mr-3 rounded-circle" src="../assets/images/widgets/opp-1.png" alt="" height="50">
                                <?php }else{?>
                                    <img class="d-flex align-self-center mr-3 rounded-circle" src="../assets/images/user_image/<?php echo getData($row['user_id'],'tbl_user','filename',$row['user_id']);?>" alt="" height="50" style="object-fit: cover;height: 50px; width: 50px;">
                                <?php }?>
                                
                                <div class="media-body align-self-center">
                                    <h4 class="mt-0 mb-2 font-16"><?php echo ucwords(getFullname($row['user_id']));?></h4>
                                    <div class="text-left">
                                       <p class="text-muted mb-2 mb-lg-0 align-self-center"><?php echo $row['remarks'];?></p>
                                    </div>
                                                                                   
                                </div><!--end media-body-->
                            </div><!--end media-->
                        </div><!--end col-->
                        <div class="col-sm-3 align-self-center">
                            <div class="text-right">
                               <p class="text-muted mb-2 mb-lg-0 align-self-center"><i class="fas fa-calendar mr-2 text-info font-14" style="color: #f1646c  !important;"></i><?php echo date("F d,Y",strtotime($row['date_added']));?></p>
                               <p class="text-muted mb-2 mb-lg-0 align-self-center"><i class="fas fa-star mr-2  font-14" style="color: #f3c74d !important;"></i><?php echo $row['star_value'];?></p>
                            </div>
                        </div><!--end col-->
                    </div>
                    </div><!--end card-body-->
                    <?php } ?>
                    
                
            </div><!--end card-->
        </div><!--end col-->
       
            <div class="col-md-3"> 
            <div class="card">
                <div class="card-body">
                    <div class="review-box text-center align-item-center">                                                                    
                        <h1><?php echo number_format(getStarValue(0),1);?></h1> 
                        <h4 class="header-title">Overall Rating</h4>  
                        <ul class="list-inline mb-0 product-review">
                            <ul class="list-inline mb-0 product-review align-self-center">
                            <?php 
                            $start=1;
                            $rating = number_format(getStarValue(0),1);
                            while($start<=5){
                                if($start<=$rating){
                                    $style='text-warning';
                                    $diff = 0 ;
                                    $half = "";
                                }else{
                                    $diff = abs($rating - $start);
                                    $style='text-none';
                                    if($diff<1 && $diff>0){
                                        $style='text-warning';
                                        $half = "-half";
                                    }else{
                                        $half = "";
                                        $style='text-none';
                                    }
                                }
                            ?>
                            <li class="list-inline-item"><i class="mdi mdi-star<?php echo $half;?> <?php echo $style;?>"></i></li>
                            <?php $start++;}
                            ?>
                            <li class="list-inline-item"><small class="text-muted">Total Review (<?php echo getStarValue(-1);?>)</small></li>
                        </ul>                                     
                    </div> 
                    <ul class="list-unstyled mt-3">
                        <li class="mb-2">
                            <span class="text-info">5 Star </span>
                            <small class="float-right text-muted ml-3 font-14"><?php echo getStarValue(5);?> </small>
                            <div class="progress mt-2" style="height:5px;">
                                <div class="progress-bar bg-secondary" role="progressbar" style="width: <?php echo getStarValuePerc(5);?>%; border-radius:5px;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li class="mb-2">
                            <span class="text-info">4 Star</span>
                            <small class="float-right text-muted ml-3 font-14"><?php echo getStarValue(4);?></small>
                            <div class="progress mt-2" style="height:5px;">
                                <div class="progress-bar bg-secondary" role="progressbar" style="width: <?php echo getStarValuePerc(4);?>%; border-radius:5px;" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li class="mb-2">
                            <span class="text-info">3 Star</span>
                            <small class="float-right text-muted ml-3 font-14"><?php echo getStarValue(3);?></small>
                            <div class="progress mt-2" style="height:5px;">
                                <div class="progress-bar bg-secondary" role="progressbar" style="width: <?php echo getStarValuePerc(3);?>%; border-radius:5px;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li class="mb-2">
                            <span class="text-info">2 Star</span>
                            <small class="float-right text-muted ml-3 font-14"><?php echo getStarValue(2);?></small>
                            <div class="progress mt-2" style="height:5px;">
                                <div class="progress-bar bg-secondary" role="progressbar" style="width: <?php echo getStarValuePerc(2);?>%; border-radius:5px;" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li>
                            <span class="text-info">1 Star</span>
                            <small class="float-right text-muted ml-3 font-14"><?php echo getStarValue(1);?></small>
                            <div class="progress mt-2" style="height:5px;">
                                <div class="progress-bar bg-secondary" role="progressbar" style="width: <?php echo getStarValuePerc(1);?>%; border-radius:5px;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                    </ul>
                    <div class="review-box text-center align-item-center">                                                                    
                        <h3><?php echo number_format(getStarValue(0)*20,2);?>%</h3> 
                        <h4 class="header-title">Satisfied Customer</h4>                                                                                                        
                    </div>  
                </div><!--end card-body-->
            </div><!--end card-->                                                                     
        </div><!--end col-->
        <?php }?>     
        
    </div><!--end row-->

</div><!-- container -->
<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaEcommerce").addClass("active");
    $(".MetricaEcommerce_list").addClass("active");
    $("#link_products").addClass("active");

     $(document).on("click",function() {

         $("#myVideo").get(0).pause(); 
    });

  });

   

  function viewDemo(){
    $("#myModal").modal('show');
  }

    $("#addCart").submit(function(e){
          e.preventDefault();
          var dealer_id = $("#modal-status-select").val();
        if(dealer_id == ''){
                noDealer();
        }else{
            $.ajax({
                url:"../ajax/addTransaction.php",
                method:"POST",
                data:$(this).serialize(),
                success: function(data){
                    if(data == 1){
                        success();
                    }else{
                        failedQuery();
                    }
                
                }
            });
        }
    });

</script>