<?php 
 $ref_num = $_GET['id'];
?>
<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Ecommerce</a></li>
                        <li class="breadcrumb-item active">Checkout</li>
                    </ol>
                </div>
                <h4 class="page-title">Checkout</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div><!--end-row-->
    <!-- end page title end breadcrumb -->
    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-3">Order Summary</h4>
                    <h4 class="header-title mt-0 mb-3">Ref # : <?php echo $ref_num;?></h4>
                    <div class="table-responsive shopping-cart">
                        <table class="table mb-0">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Quantity</th>                                                        
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                  <?php 
                               $fetch = mysql_query("SELECT * FROM tbl_transaction where user_id='$id' and ref_num='$ref_num' ORDER BY `tbl_transaction`.`date_added` DESC");
                                $subtotal = 0;
                                $shipping_fee = 0;
                                while($row = mysql_fetch_array($fetch)){
                                    $total = $row['qty']*getData($row['product_id'],'tbl_product','price','product_id');
                                     $filename = getData($row['product_id'],'tbl_product','filename','product_id');
                                    $subtotal+=$total;

                                    // if ($row['ship_opt']==0) {
                                    //     $shipping_fee = 100;
                                    // }else if ($row['ship_opt']==1){
                                    //     $shipping_fee = 150;
                                    // }


                                ?>

                                <tr>
                                    <td>
                                          <img src="../assets/images/products/<?php echo $filename;?>" alt="" height="52">
                                        <p class="d-inline-block align-middle mb-0 product-name"><?php echo substr(getData($row['product_id'],'tbl_product','product_name','product_id'),0,20)?></p> 
                                    </td>
                                    <td>
                                        <?php echo $row['qty'];?>
                                    </td>
                                    <td><?php echo number_format($total,2);?></td>                                                        
                                </tr>  
                                <?php }?>                                                  
                            </tbody>
                        </table>
                    </div><!--end re-table-->
                    <div class="total-payment">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td class="payment-title">Subtotal</td>
                                    <td>&#8369; <?php echo number_format($subtotal,2);?></td>
                                <!-- </tr>
                                <tr>
                                    <td class="payment-title">Shipping</td>
                                    <td>
                                        &#8369; <?php echo $shipping_fee;?>
                                    </td>
                                </tr> -->
                                <tr>
                                    <td class="payment-title">Total</td>
                                    <td class="text-dark"><strong> &#8369; <?php echo number_format($subtotal,2);?></strong></td>
                                </tr>
                            </tbody>
                        </table><!--end table-->
                    </div><!--end total-payment-->
                </div><!--end card-body-->
            </div><!--end card-->
        </div><!--end col-->

        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-3">Delivery Address</h4>
                   <!--  <form class="mb-0" id="checkoutForm"> -->
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden" class="form-control" id="ref_num" name="ref_num" required="" readonly value="<?php echo $ref_num;?>">

                                <div class="form-group">
                                    <label>First Name <small class="text-danger font-13">*</small></label>
                                    <input type="text" class="form-control" id="firstname" required="" readonly value="<?php echo getData($id,'tbl_user','fname','user_id')?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Last Name <small class="text-danger font-13">*</small></label>
                                    <input type="text" class="form-control" id="lastname" required="" readonly value="<?php echo getData($id,'tbl_user','lname','user_id')?>">
                                </div>
                            </div><!--end col-->                                                
                        </div><!--end row-->
                        <div class="row">
                            <div class="col-md-12">                            
                                <div class="form-group">
                                    <label>Delivery Address <small class="text-danger font-13">*</small></label>
                                    <input type="text" id="del_add" class="form-control" required="" readonly value="<?php echo getData($id,'tbl_user','address','user_id')?>">
                                    <label id="response"></label>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                        <div class="row">
                          
                            <div class="col-md-6">                            
                                <div class="form-group">
                                    <label>Email Address <small class="text-danger font-13">*</small></label>
                                    <input type="email" class="form-control" required="" readonly value="<?php echo getData($id,'tbl_user','email','user_id')?>">
                                </div>
                            </div><!--end col-->
                            <div class="col-md-6">                            
                                <div class="form-group">
                                    <label>Mobile No <small class="text-danger font-13">*</small></label>
                                    <input type="text" class="form-control" required="" readonly value="<?php echo getData($id,'tbl_user','contact_number','user_id')?>">
                                </div>
                            </div><!--end col-->
                        </div><!--end row--> 
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-0">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="horizontalCheckbox" name="horizontalCheckbox" data-parsley-multiple="groups" data-parsley-mincheck="2" required="" >
                                        <label class="custom-control-label" for="horizontalCheckbox">Confirm Shipping Address</label>
                                    </div>
                                    <button type='button' onclick='finalCheckout()' class="btn btn-success px-3">Check Out</button>

                                </div>              
                            </div><!--end col-->
                        </div><!--end row-->                                            
                    <!-- </form> --><!--end form-->
                   
                </div><!--end card-body-->
            </div><!--end card-->
        </div><!--end col-->
    </div><!--end row-->

</div><!-- container -->
<script type="text/javascript">
    function finalCheckout(){
        var ref_num = $("#ref_num").val();
        var del_add = $("#del_add").val();
        if($("input[name='horizontalCheckbox']").is(':checked')){
            if(del_add == ''){
                Swal.fire(
                    'Opps!',
                    'Please Update your address to continue!',
                    'warning'
                  );
            }else{
                $.post("../ajax/finish_checkout.php", {
                    ref_num: ref_num
                }, function(data){
                    if(data == 1){
                        alert_notif('Done!','Successfully Checkout','success');
                        setTimeout(function(){
                         window.location.replace("../ecommerce/index.php?page=pending");
                        },1000)
                    }else{
                        failedQuery();
                    }
                })
            }
        }else{
            Swal.fire(
                'Opps!',
                'Please Confirm your shipping address!',
                'warning'
              );
        }
    }
  $(document).ready(function(){
    $(".MetricaOthers").addClass("active");
    $(".MetricaOthers_list").addClass("active");
    $("#link_pending").addClass("active");
    var delivery_add = $("#del_add").val();
    if(delivery_add == ''){
        $("#del_add").css("border","1px solid red");
        $("#del_add").addClass("animated flash infinite");
        $("#response").html("Please Update your Address in your profile");
        $("#response").css("color","red");
    }else{
        $("#del_add").css("");
        $("#del_add").removeClass("animated flash infinite");
        $("#response").html("");
    }
});
</script>