<?php 
 $ref_num = $_GET['id'];
 $dealer_id = getData($ref_num,'tbl_transaction','dealer_id','ref_num');

?>
<style type="text/css">
    .project-dash-activity{
        min-height: 0px !important;
    }
</style>
<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Transaction</a></li>
                        <li class="breadcrumb-item active">Review</li>
                    </ol>
                </div>
                <h4 class="page-title">Review</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div><!--end-row-->
    <!-- end page title end breadcrumb -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                  <div class="card-body" id="review_body">
                      <h4 class="header-title mt-0 mb-3"><span class="mdi mdi-truck-check"></span>Ref # : <?php echo $ref_num;?></h4>
                       <?php 
                               $fetch = mysql_query("SELECT * FROM tbl_transaction where user_id='$id' and ref_num='$ref_num' ORDER BY `tbl_transaction`.`date_added` DESC");
                                $subtotal = 0;
                                $shipping_fee = 0;
                                $num = mysql_num_rows($fetch);
                                $count=1;
                                while($row = mysql_fetch_array($fetch)){
                                $check_review = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_review where ref_num='$ref_num' and product_id='$row[product_id]'"));
                                if($check_review[0]==0){
                                ?>
                    <form id="FormReview<?php echo $count;?>">
                    <div class="table-responsive" class="formBody<?php echo $count;?>">
                        <h6><div class="table-responsive">
                            <div class="d-lg-flex justify-content-between">                                                
                                <div class="media mb-3 mb-lg-0">
                                    <img src="../assets/images/users/user-2.jpg" class="mr-3 thumb-md align-self-center rounded-square" alt="...">
                                    <div class="media-body align-self-center"> 
                                        <h5 class="mt-0 mb-1"><?php echo ucwords(getData($row['company_id'],'tbl_company','company_name','company_id'));?></h5>
                                        <p class="text-muted mb-0"><i class="fab fa-opencart mr-2 text-info"></i> <?php echo ucwords(getData($row['product_id'],'tbl_product','product_name','product_id'));?></p>    
                                    </div><!--end media body-->
                                </div> <!--end media-->
                            </div>
                            <input type="hidden" name="product_id" value="<?php echo $row['product_id'];?>">
                            <input type="hidden" name="ref_num" value="<?php echo $ref_num;?>">
                            <input type="hidden" name="count"  class="count" value="<?php echo $count;?>">

                            <hr>
                            <div class="col-lg-12">
                                <div class="p-4" style="font-size: 36px;text-align: center;">
                                    <select id="example-fontawesome<?php echo $count;?>" name="star_value">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5" selected="">5</option>
                                    </select>                                                              
                                </div>
                            </div><!--end col-->
                            <br>
                            <textarea id="textarea" class="form-control" maxlength="225" rows="3" placeholder="Share your experience and help others make better choices!" required="" name="remarks"></textarea>
                        </div>
                        <br> <br>
                         <button class="btn btn-primary btn-round btn-block waves-effect waves-light" type="submit">Submit<i class="fas fa-check-circle ml-1"></i></button>
                    </div>
                </form>
                    <?php $count++;}} ?>
                </div><!--end card-body-->
                 <hr>
            </div><!--end card-->
        </div><!--end col-->
    </div><!--end row-->

</div><!-- container -->
<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaOthers").addClass("active");
    $(".MetricaOthers_list").addClass("active");
    $("#link_finished").addClass("active");


    var num = <?php echo $num;?>;
    var start = 1;
    while(start<=num){

     $('#example-fontawesome'+start).barrating({
      theme: 'fontawesome-stars',
      showSelectedRating: false
    });


     $("#FormReview"+start).submit(function(e){
    e.preventDefault();
     var count = $(this).find(".count").val();

     $.ajax({
        url:"../ajax/addReview.php",
        method:"POST",
        data:$(this).serialize(),
        success: function(data){

        if(data == 1){
            alert_notif("Done","Thank you for writing a review","success");
            setTimeout(function(){
                $("#FormReview"+count).remove();
            },1000);
         }else if(data == 2){
            alert_notif("Sorry","You have already write a review","warning");
        }else{
            failedQuery();
        }
        }
      });
    });


     start++;
    }




});
</script>