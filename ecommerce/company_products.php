<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item active">Products</li>
                    </ol>
                </div>
                <h4 class="page-title">Products >> <?=getData($company_id,'tbl_company','company_name','company_id')?></h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
    
    <div class="row">  
    <?php if($status == 'A'){?>
        <div class="col-lg-12 ">
            <a class="btn btn-primary px-4 btn-rounded float-right mt-0 mb-3  waves-effect waves-light" href="index.php?page=add-product&id=0">+ Add New Product</a>
        </div>
    <?php }?>
        
        <div class='col-lg-12 card'>
            <table class="table table-striped mb-0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th style='width:120px;'>Date Added</th>
                        <th style='width:100px;'>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    
                    $data = GETALLPRODUCTS($company_id);
                    if(count($data) > 0){
                        foreach($data as $prd){
                            $image = ($prd['prd_image'] == '')?"img-1.png":$prd['prd_image'];
                          
                            echo "<tr>";
                                echo "<td>".$prd['count']."</td>";
                                echo "<td>".$prd['prd_name']."</td>";
                                echo "<td><img src='../assets/images/products/".$image."' alt='' class='rounded-circle thumb-xs mr-1'></td>";
                                echo "<td>".substr($prd['prd_desc'], 0, 50)."...</td>";
                                echo "<td>".number_format($prd['prd_price'], 2)."</td>";
                                echo "<td>".date("M d, Y h:i A", strtotime($prd['prd_date']))."</td>";
                                if($status == 'A'){
                                      echo "<td>
                                        <button class='btn btn-sm btn-primary' onclick='editProduct(".$prd['product_id'].")'>
                                        <span class='fa fa-edit'></span>
                                        </button>
                                        <button id='btnDlt".$prd['product_id']."' class='btn btn-sm btn-danger' onclick='deleteProduct(".$prd['product_id'].")'>
                                        <span class='fa fa-trash'></span>
                                        </button>
                                      </td>";
                                }else{
                                     echo "<td>
                                        <button class='btn btn-sm btn-primary' onclick='viewProduct(".$prd['product_id'].")'>
                                        <span class='fa fa-eye'></span>
                                        </button>
                                      </td>";
                                }
                              
                            echo "</tr>";
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>


    </div><!--end row-->
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaCRM").addClass("active");
    $(".MetricaCRM_list").addClass("active");
    $("#link_company_products").addClass("active");
  });
  function editProduct(prdID){
      window.location = 'index.php?page=add-product&id='+prdID;
  }
   function viewProduct(prdID){
      window.location.replace("index.php?page=viewProduct&id="+prdID);
  }
  function deleteProduct(prdID){
      var p_action = 'delete';
     $("#btnDlt"+prdID).prop("disabled", true);
     $("#btnDlt"+prdID).html("<span class='fa fa-spin fa-spinner'></span>");
     $.post("../ajax/CRUD_product.php", {
        prdID: prdID,
        p_action: p_action
     }, function(data){
        if(data == 1){
            custom_alert("All Good!","Product was successfully deleted.","success");
        }else{
            custom_alert("Aw Snap!","Unable to finish transaction, Please Try Again.","success"); 
        }
     })
  }
  function productdetails(id){
        window.location.replace("index.php?page=productDetail&id="+id);
  }
</script>