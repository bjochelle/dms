 <!-- Page Content-->
<div class="page-content">

    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="float-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Accept Deals</a></li>
                            <li class="breadcrumb-item active">Deals</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Deals</h4>
                </div><!--end page-title-box-->
            </div><!--end col-->
        </div><!--end row-->
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Deals Transactions</h4>

                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>Customer Name</th>
                                <th>Product</th>
                                <th>Address</th>
                                <th>Date Added</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div><!-- container -->
</div>
<!-- end page content -->

<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaCRM").addClass("active");
    $(".MetricaCRM_list").addClass("active");
    $("#link_deals").addClass("active");
      getData();
  });

function viewDetails(id){
    var ref_num = $("#view"+id).val();
    window.location.replace("index.php?page=viewTransaction&id="+ref_num);
}

function dl(id){
    var ref_num = $("#cancel"+id).val();
     $.ajax({
        url:"../ajax/cancelTrans.php",
        method:"POST",
        data:{ref_num:ref_num
        },
        success: function(data){
           if(data == 1){
                success_cancel();
                setTimeout(function(){
                 window.location.replace("../ecommerce/index.php?page=cancelled");
                },1000)
            }else{
                failedQuery();
            }
           
        }
    });
}

function confirm(id){
    var ref_num = $("#view"+id).val();
     $.ajax({
        url:"../ajax/confirmDeal.php",
        method:"POST",
        data:{ref_num:ref_num
        },
        success: function(data){
           if(data == 1){
                alert_notif("All Good!","Deal was Successfully Confirmed.","success");
                getData();
            }else{
                failedQuery();
            }
        }
    });
}

    function getData(){
      var table = $('#datatable').DataTable();
      table.destroy();
      var status = 'P';
      $("#datatable").dataTable({
        "processing":true,
        "ajax":{
          "method":"POST",
          "url":"../ajax/datatables/dt_deals.php",
          "data":{
            status:status
          },
          "dataSrc":"data"
        },
        "columns":[
          {
            "data":"customer_name"
          },
          {
            "data":"ref_num"
          },
          {
            "data":"address"
          },
          {
            "data":"date"
          },          
          {
            "mRender": function(data,type,row){
              return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Details' value='"+row.ref_num+ "' onclick='viewDetails("+row.id+")' id='view"+row.id+"'><span class='fa fa-eye'></span></button><button class='btn btn-success btn-sm' data-toggle='tooltip' title='Confirm' value='"+row.ref_num+ "' onclick='confirm("+row.id+")' id='confirm"+row.id+"'><span class='fa fa-check'></span></button></center>";
            }
          }
        ]
      });
    }

</script>