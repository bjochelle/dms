 <!-- Page Content-->
<div class="page-content">

    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="float-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Transactions</a></li>
                            <li class="breadcrumb-item active">Pending</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Pending</h4>
                </div><!--end page-title-box-->
            </div><!--end col-->
        </div><!--end row-->
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Pending Transactions</h4>

                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>Ref #</th>
                                <th>Total Price</th>
                                <th>Date Added</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div><!-- container -->
</div>
<!-- end page content -->

<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaOthers").addClass("active");
    $(".MetricaOthers_list").addClass("active");
    $("#link_pending").addClass("active");
      getData();


    
  });

function viewDetails(id){
    var ref_num = $("#view"+id).val();
    window.location.replace("index.php?page=viewTransaction&id="+ref_num);
}

function ConfirmDelete(id){
     deleteData(id);
  }

function dl(id){
    var ref_num = $("#cancel"+id).val();
     $.ajax({
        url:"../ajax/cancelTrans.php",
        method:"POST",
        data:{ref_num:ref_num
        },
        success: function(data){
           if(data == 1){
                success_cancel();
                setTimeout(function(){
                 window.location.replace("../ecommerce/index.php?page=cancelled");
                },1000)
            }else{
                failedQuery();
            }
           
        }
    });

}

  


    function getData(){
      var table = $('#datatable').DataTable();
      table.destroy();
      var status = 'P';
      $("#datatable").dataTable({
        "processing":true,
        "ajax":{
          "method":"POST",
          "url":"../ajax/datatables/dt_trans.php",
          "data":{
            status:status
          },
          "dataSrc":"data"
        },
        "columns":[
          {
            "data":"ref_num"
          },
          {
            "data":"total_price"
          },
          {
            "data":"date"
          },          
          {
            "mRender": function(data,type,row){
              return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Details' value='"+row.ref_num+ "' onclick='viewDetails("+row.id+")' id='view"+row.id+"'><span class='fa fa-eye'></span></button><button class='btn btn-danger btn-sm' data-toggle='tooltip' title='Cancel' value='"+row.ref_num+ "' onclick='ConfirmDelete("+row.id+")' id='cancel"+row.id+"'><span class='fooicon fooicon-trash'></span></button></center>";
            }
          }
        ]
      });
    }

</script>