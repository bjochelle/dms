<?php 
  $msg = $_GET['id'];

  $receiver_id = mysql_fetch_array(mysql_query("SELECT sender_id FROM tbl_messages WHERE message_id = '$msg'"));
  $rcvID = $receiver_id[0];
  $getName = mysql_fetch_array(mysql_query("SELECT * FROM tbl_user WHERE `user_id` = '$rcvID'"));
  $name = $getName['fname']." ".$getName['lname'];
?>
 <style>
     .cg :hover {
      background-color: #80868e;
  }

*,
*::before,
*::after {
  margin: 0;
  border: 0;
  padding: 0;
  word-wrap: break-word;
  box-sizing: border-box;
}

.message-sent,
.message-received {
  clear: both;
}
.message-sent::before,
.message-received::before,
.message-sent::after,
.message-received::after {
  content: '';
  display: table;
}

[class^='grid-'] {
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-flex-wrap: wrap;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
}
[class^='grid-'] {
  -webkit-flex-direction: row;
  -ms-flex-direction: row;
  flex-direction: row;
}
.grid-message >[class^='col-'] {
  margin-top: 1em;
  margin-right: 1em;
}
.grid-message >[class^='col-']:nth-child(-n + 1) {
  margin-top: 0;
}
.grid-message >[class^='col-']:nth-child(1n) {
  margin-right: 0;
}
.col-message-sent {
  margin-left: calc(8.33333333% + 0.08333333em) !important;
}
.col-message-received {
  margin-right: calc(8.33333333% + 0.08333333em) !important;
}
.col-message-sent,
.col-message-received {
  width: calc(91.66666667% - 0.08235677em);
}
.message-sent,
.message-received {
  margin-top: 0.0625em;
  margin-bottom: 0.0625em;
  padding: 0.25em 1em;
}
.message-sent p,
.message-received p {
  margin: 0;
  line-height: 1.5;
}
.message-sent {
  float: right;
  color: white;
  background-color: gray;/*dodgerblue;*/
  border-radius: 1em 0.25em 0.25em 1em;
}
.message-sent:first-child {
  border-radius: 1em 1em 0.25em 1em;
}
.message-sent:last-child {
  border-radius: 1em 0.25em 1em 1em;
}
.message-sent:only-child {
  border-radius: 1em;
}
.message-received {
  float: left;
  color: black;
  background-color: #c5c5c5;
  border-radius: 0.25em 1em 1em 0.25em;
}
.message-received:first-child {
  border-radius: 1em 1em 1em 0.25em;
}
.message-received:last-child {
  border-radius: 0.25em 1em 1em 1em;
}
.message-received:only-child {
  border-radius: 1em;
}
.col-message-sent {
  margin-top: 0.25em !important;
}
.col-message-received {
  margin-top: 0.25em !important;
}
.message {
  min-height: 53.33203125em;
  max-width: 30em; /* !!!!!!!!!! COMMENT OUT THIS LINE TO MAKE IT FULL WIDTH !!!!!!!!!! */
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
}
.btn_hover:hover{
  background-color: #80868e;
  color: white;
}
.fstMultipleMode.fstActive, .fstMultipleMode.fstActive .fstResults {
    box-shadow: 0 0.2em 0.2em rgba(0, 0, 0, 0.1);
    border-radius: 5px 5px 5px 5px;
    width: 77%;
}
.fstElement {
    display: inline-block;
    position: relative;
    border: 1px solid #D7D7D7;
    box-sizing: border-box;
    color: #232323;
    font-size: 1.1em;
    background-color: #fff;
    border-radius: 5px 5px 5px 5px;
    width: 77%;
  }
  .fstResults{
        width: 100%;
  }
  .fs-wrap{
    width: 77%;
  }
  .fs-label-wrap{
    height: 38px;
  }
  .fs-dropdown{
    width: 77%;
  }
  .fs-label-wrap .fs-label {
    padding: 10px 22px 11px 15px;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    text-align: center;
  }
 </style>
 <!-- Page Content-->
 <div class="page-content">

<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Messages</a></li>
                        <li class="breadcrumb-item active">Messages</li>
                    </ol>
                </div>
                <h4 class="page-title">Messages</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div><!--end row-->
    <!-- end page title end breadcrumb -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="mt-0 header-title">Messages</h4>
                    <div class='row' style="height: 445px;">
                <div class="col-md-4">
                  <div class="" style="padding:10px;border: 1px solid #ccc;">
                      Users
                  </div>
                  <div class="" style="height: 406px;padding: 0px;border: 1px solid #ccc;">
                      <div class="box box-warning direct-chat direct-chat-warning" style="height: 100%;border-top: 0px;">
                          <div class="box-body" style="height: 100%;">
                            <div class="direct-chat-messages" style="height: 100%;padding: 0px; overflow-y: scroll;">
                                <ul class="products-list product-list-in-box" style="">
                                    <input type="hidden" id="user_session_id" value="<?php echo $id; ?>">
                                    <?php 
                                      $where = ($status == 'D')?" status = 'C'":" status = 'D'";
                                    $getUserstoChat = mysql_query("SELECT * FROM tbl_user WHERE $where ");
                                  
                                    while($fetchUsers = mysql_fetch_array($getUserstoChat)){
                                        $name = $fetchUsers['fname']." ".$fetchUsers['lname'];
                                    ?>
                                    <li class="item cg" style="padding:0px;border-bottom: 1px solid #ddd; list-style: none;" onclick="user_list('<?php echo $fetchUsers['user_id'] ?>','<?php echo $name ?>')">
                                      <div class="product-info" id='name<?php echo $userid ?>' style="padding: 10px;margin-left: 0px;">
                                        <a href="javascript:void(0)" class="product-title"><?php echo $name; ?></a>
                                        <input type="hidden" id="cgID_checker" value="">

                                        <span class="product-description" style="margin-top: -5px;">
                                          </span>
                                      </div>
                                    </li>
                                    <?php } ?>
                                </ul>
                              </div>
                          </div>
                      </div>
                    </div>
                 </div>
                <div class="col-md-8" style='margin-left: -25px'>
                <div class="" style="height:409px;padding: 0px;border: 1px solid #ccc;">
                    <!-- DIRECT CHAT -->
                    <div class="box box-warning direct-chat direct-chat-warning" style="height: 100%;border-top: 0px;">
                      <!-- /.box-header -->
                      <div class="box-body" style="height: 100%;">
                        <div class='col-md-12' style="background-color: #c5c5c5;"><center><label id='names'></label></center></div>
                        <!-- Conversations are loaded here -->
                        <div class="grid-message" style="overflow-x: auto;height: 90%;padding: 10px;" id="msg">

                        </div>
                        <!--/.direct-chat-messages-->
                      </div>
                      <!-- /.box-body -->
                      <div class="box-footer">
                          <div class="input-group">
                            <input type="hidden" id="sender_id" value="<?php echo $id;?>">
                            <input type="hidden" id="receiver_id" value='<?php echo $rcvID; ?>'>
                            <input type="hidden" id="name" value='<?php echo $name; ?>'>
                            <input type="text" id="chatContent" style='border: 1px solid #cccccc;' row="3" name="message"  autocomplete="off" placeholder="Type a message ..." class="form-control">
                            <span class="input-group-btn">
                                
                                <button type="button" class="btn btn-primary btn-flat" id="send_btn" onclick="sendMsg()">Send</button>
                              </span>
                          </div>
                      </div>
                      <!-- /.box-footer-->
                    </div>
                </div>
             </div>
            </div>
          </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
</div><!-- container -->
</div>
<!-- end page content -->

<script type="text/javascript">
$(document).ready(function(){
    $(".MetricaCRM").addClass("active");
    $(".MetricaCRM_list").addClass("active");
    var receiver_id = $("#receiver_id").val();
    var name = $("#name").val();
    $("#names").html(name);
    loadChat(receiver_id);

});
   

 $(document).keypress(function(e) {
      if(e.which == 13) {
          sendMsg();
      }
      var receiver_id = $("#receiver_id").val();
      loadChat(receiver_id);
  });
function scrolling(){
    var objDiv = document.getElementById("msg");
    objDiv.scrollTop = objDiv.scrollHeight;
  }
function user_list(id,name){
    $("#names").html(name);
    $("#msg").html("<h4 class='col-md-6 col-md-offset-3' style='text-align: center; margin-top: 180px;'><span class='fa fa-spinner'></span> Loading Messages...</h4>");
    $("#receiver_id").val(id);
    setTimeout(scrolling, 0);
    loadChat(id);
  }
  function loadChat(r_id){
   // var interval = setInterval( function(){
      viewMsg(r_id);
      setTimeout(scrolling, 500);
    //}, 10000);
  }
  function viewMsg(rec_id){
    var sender_id = $("#user_session_id").val();
    $.post('../ajax/getMessage.php', {
      rec_id: rec_id,
      sender_id:sender_id
    },
    function(data){

      var msg = JSON.parse(data);
      $("#msg").html(msg.msg_content);
    });
  }
  function sendMsg(){
    var sender_id = $("#sender_id").val();
    var receiver_id = $("#receiver_id").val();
    var chatContent = $("#chatContent").val();
    $("#send_btn").prop("disabled", true);
    $("#send_btn").html("<span class='fa fa-spin fa-spinner'></span>");
    if(sender_id == '' || receiver_id == '' || chatContent == ''){
      alert('Failed');
    }else{
      $.post('../ajax/send_message.php', {
      sender_id: sender_id,
      receiver_id: receiver_id,
      chatContent: chatContent
    }, function(data){
      if(data != 0){
        $("#chatContent").val("");
        loadChat(data);
      }else{
        alert('Failed');
      }
      setTimeout(scrolling, 10);
      $("#send_btn").prop("disabled", false);
      $("#send_btn").html("<span class=''></span> Send");
    });
    }
  }
</script>