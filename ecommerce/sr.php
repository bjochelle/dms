 <!-- Page Content-->
<div class="page-content">

    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="float-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Report</a></li>
                            <li class="breadcrumb-item active">Sales Report</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Sales Report</h4>
                </div><!--end page-title-box-->
            </div><!--end col-->
        </div><!--end row-->
        <!-- end page title end breadcrumb -->

          <div class="row">
              <div class="col-lg-12 mx-auto">
                  <div class="card">
                      <div class="card-body"> 
                        <div class="row d-print-none">
                            <div class="col-md-3">
                                <div class="">
                                   Start Date : <input type="date" id="sd">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="">
                                  End Date :   <input type="date" id="ed">
                                </div>
                            </div> 

                            <?php if($status != 'A'){?>
                               <input type="hidden" id="company_id" value="<?php echo $company_id;?>">

                            <?php }else{?>
                              <div class="col-md-3">
                                <div class="">
                                  Branch :  
                                  <select class="custom-select select2" id="company_id" name="company_id">
                                        <option selected="">Select</option>
                                        <option value="-1">All Branch</option>
                                        <?php 
                                        $fetch_company = mysql_query("SELECT * FROM `tbl_company`");
                                        while($row_company = mysql_fetch_array($fetch_company)){
                                        echo '<option value="'.$row_company['company_id'].'">'.ucwords($row_company['company_name']).'</option>';
                                        }?>
                                    </select>
                                </div>
                            </div>

                            <?php }?>
                           
                             
                           <div class="col-md-3">
                              <div class="float-right">
                                    <button href="#" class="btn btn-primary" onclick="gen()" id="btn_gen">Generate Report</button>
                                    <a href="javascript:window.print()" class="btn btn-info"><i class="fa fa-print"></i></a>
                                </div>
                            </div>  
                        </div> 
                              <hr>
                          <div id="report_data">
                                    
                          </div>
                      </div><!--end card-body-->
                  </div><!--end card-->
              </div><!--end col-->
          </div><!--end row-->
    </div><!-- container -->
</div>
<!-- end page content -->

<script type="text/javascript">
  $(document).ready(function(){
    $(".Report").addClass("active");
    $(".Report_list").addClass("active");
    $("#link_sr").addClass("active");
  });

  function gen(){
    var sd = $("#sd").val();
    var ed = $("#ed").val();
    var company_id = $("#company_id").val();


    if(sd == "" || ed == "" || company_id == ""){
      alert_notif("Ooppss!","Please fill out the form.","warning");
    }else{
      $("#btn_gen").prop("disabled",true);
      $("#report_data").html('<center><div class="spinner-border thumb-lg text-primary" role="status">  </div> <h3>Please Wait</h3></center>');
      $.ajax({
        url:"../ajax/sales_report.php",
        method:"POST",
        data:{
          sd:sd,
          ed:ed,
          company_id:company_id
        },success:function(data){
          $("#btn_gen").prop("disabled",false);
          $("#report_data").html(data)
        }
      })
    }
}
</script>