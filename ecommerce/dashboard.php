<?php 

 if($status == 'A'){
    $count_finished = mysql_num_rows(mysql_query("SELECT * from tbl_transaction  where status='F' group by ref_num "));
    $count_pending = mysql_num_rows(mysql_query("SELECT * from tbl_transaction  where status='P' group by ref_num "));
}else{
    $count_finished = mysql_num_rows(mysql_query("SELECT * from tbl_transaction  where status='F' AND company_id='$company_id' group by ref_num "));
    $count_pending = mysql_num_rows(mysql_query("SELECT * from tbl_transaction  where status='P' AND company_id='$company_id' group by ref_num ")); 
}

$count_happy = mysql_num_rows(mysql_query("SELECT * from tbl_review  where star_value='5'"));
$count_customer = mysql_num_rows(mysql_query("SELECT * from tbl_user  where status='C'"));

?>
<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">Dashboard</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
            <!-- end page title end breadcrumb -->
    <div class="row">
        <div class="col-lg-12 col-xl-8">
            <div class="card">
                <div class="card-body">            
                    <h4 class="mt-0 header-title">Sales Chart</h4>          
                    <canvas id="bar" height="300"></canvas>            
                </div><!--end card-body-->
            </div><!--end card-->
        </div> <!-- end col -->
        <div class="col-lg-12 col-xl-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="card crm-data-card">
                        <div class="card-body"> 
                            <div class="row">
                                <div class="col-4 align-self-center">
                                    <div class="data-icon">
                                        <i class="far fa-smile rounded-circle bg-soft-warning"></i>
                                    </div>
                                </div><!-- end col-->
                                <div class="col-8">
                                    <h3><?php echo $count_happy;?></h3>
                                    <p class="text-muted font-14 mb-0">Happy Customers</p>
                                </div><!-- end col-->
                            </div><!-- end row-->                                                                                  
                        </div><!--end card-body--> 
                    </div><!--end card-->   
                </div><!-- end col-->
                <div class="col-md-12">
                    <div class="card crm-data-card">
                        <div class="card-body"> 
                            <div class="row">
                                <div class="col-4 align-self-center">
                                    <div class="data-icon ">
                                        <i class="far fa-user rounded-circle bg-soft-success"></i>
                                    </div>
                                </div><!-- end col-->
                                <div class="col-8">
                                    <h3><?php echo $count_customer;?></h3>
                                    <p class="text-muted font-14 mb-0">Total Customers</p>
                                </div><!-- end col-->
                            </div><!-- end row-->
                        </div><!--end card-body--> 
                    </div><!--end card-->   
                </div><!-- end col-->
            </div><!--end row-->

            <div class="row">
                <div class="col-md-6">
                    <div class="card crm-data-card">
                        <div class="card-body"> 
                            <div class="row">
                                <div class="col-4 align-self-center">
                                    <div class="data-icon">
                                        <i class="far fa-handshake rounded-circle bg-soft-secondary"></i>
                                    </div>
                                </div><!-- end col-->
                                <div class="col-8">
                                    <h3><?php echo $count_pending;?></h3>
                                    <p class="text-muted font-14 mb-0">New Deals</p>
                                </div><!-- end col-->
                            </div><!-- end row-->                                                                                     
                        </div><!--end card-body--> 
                    </div><!--end card-->                                      
                </div><!-- end col-->
                <div class="col-md-6">
                    <div class="card crm-data-card">
                        <div class="card-body"> 
                            <div class="row">
                                <div class="col-4 align-self-center">
                                    <div class="data-icon">
                                        <i class="far fa-registered rounded-circle bg-soft-pink"></i>
                                    </div>
                                </div><!-- end col-->
                                <div class="col-8">
                                    <h3><?php echo $count_finished;?></h3>
                                    <p class="text-muted font-14 mb-0">Finished Deals</p>
                                </div><!-- end col-->
                            </div><!-- end row-->
                        </div><!--end card-body--> 
                    </div><!--end card-->   
                </div><!-- end col-->
            </div><!--end row-->
        </div><!--end col-->
        <div class="col-lg-12 col-xl-8">
            <div class="card">
                <div class="card-body">            
                    <h4 class="mt-0 header-title">Inventory for Today</h4>          
                     <?=getInventoryforToday(); ?>       
                </div><!--end card-body-->
            </div><!--end card-->
        </div> <!-- end col -->
        <div class="col-lg-12 col-xl-4">
            <div class="card">
                <div class="card-body">            
                    <h4 class="mt-0 header-title">Top Selling Products</h4>          
                     <?=getTopSellingProducts(); ?>       
                </div><!--end card-body-->
            </div><!--end card-->
        </div> <!-- end col -->
    </div><!-- container -->
</div>
<script src="../assets/plugins/chartjs/chart.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaCRM").addClass("active");
    $(".MetricaCRM_list").addClass("active");
    $("#link_dashboard").addClass("active");

    showGraph();
//     // create initial empty chart
// var ctx_live = document.getElementById("bar");
// var myChart = new Chart(ctx_live, {
//   type: 'bar',
//   data: {
//     labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November","December"],
//     datasets: [{
//       data: [],
//        label: "Sales Analytics (2020)",
//         backgroundColor: "rgba(74, 199, 236, 0.4)",
//         borderColor: "#4ac7ec",
//         borderWidth: 2,
//         barPercentage: 0.3,
//         categoryPercentage: 0.5,
//         hoverBackgroundColor: "rgba(74, 199, 236, 0.7)",
//         hoverBorderColor: "#4ac7ec"
//     }]
//   },
//   options: {
//     responsive: true,
//     title: {
//       display: true,
    
//     },
//     legend: {
//       display: false
//     },
//     scales: {
//       yAxes: [{
//         ticks: {
//           beginAtZero: true,
//         }
//       }]
//     }
//   }
// });

// $.ajax({
//     type: 'POST', //post method
//     url: '../ajax/getSales.php', //ajaxformexample url
//     dataType: "json",
//     success: function (result, textStatus, jqXHR)
//     {
//         chart.data = result;
//         chart.update();
//     }
// });

  });// used for example purposes



        function showGraph()
        {
            {
                $.post("../ajax/getSales.php",
                function (data)
                {
                    console.log(data);
                
                    var chartdata = {
                        labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November","December"],
                        datasets: [
                            {
                                label: "Sales Analytics (2020)",
                                backgroundColor: "rgba(74, 199, 236, 0.4)",
                                borderColor: "#4ac7ec",
                                borderWidth: 2,
                                barPercentage: 0.3,
                                categoryPercentage: 0.5,
                                hoverBackgroundColor: "rgba(74, 199, 236, 0.7)",
                                hoverBorderColor: "#4ac7ec",
                                data: data  
                            }
                        ]
                    };

                    var graphTarget = $("#bar");

                    var barGraph = new Chart(graphTarget, {
                        type: 'bar',
                        data: chartdata
                    });
                });
            }
        }


</script>