<?php 

    if($status == 'A'){?>
    <style type="text/css">
        /* .footable-editing{
            display: none;
        }  */
    </style>

<?php }?>

<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">EDMS</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">CRM</a></li>
                        <li class="breadcrumb-item active">SUPPLY</li>
                    </ol>
                </div>
                <h4 class="page-title">SUPPLY</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0">Supply List</h4>
                    <table id="footable-3" class="table mb-0" data-paging="true" data-filtering="true" data-sorting="true">
                        <thead>
                            <tr>
                                <th data-name="supply_id" data-breakpoints="xs" data-type="number">ID</th>
                                <th data-name="company_id" data-breakpoints="xs">Company</th>
                                <th data-name="product_id">Product Name</th>
                                <th data-name="qty" data-breakpoints="xs">Quantity</th>
                                <th data-name="date_added" data-breakpoints="xs">Date Added</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $data = GETALLSUPPLY();
                    if(count($data) > 0){
                        foreach($data as $prd){
                          
                            echo "<tr>";
                                echo "<td>".$prd['supply_id']."</td>";
                                echo "<td>".$prd['company_id']."</td>";
                                echo "<td>".$prd['product_id']."</td>";
                                echo "<td>".$prd['qty']."</td>";
                                echo "<td>".$prd['date_added']."</td>";

                                //echo "<td>".date('F d,Y',strtotime($prd['date_added']))."</td>";

                              
                            echo "</tr>";
                        }
                    }
                            ?>
                        </tbody>
                    </table><!--end table-->

                    <!--Editor-->
                    <div class="modal fade" id="editor-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title">
                    
                        <div class="modal-dialog" role="document">
                            <form class="modal-content form-horizontal" id="editor">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="editor-title">Add Rasdasdasow</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>                                                            
                                </div>
                                <div class="modal-body">
                                    <div class="form-group row" id="ids" style='display:none'>
                                        <label for="supply_id" class="col-sm-3 control-label">ID</label>
                                        <div class="col-sm-9">
                                            <input type="text" readonly class="form-control" id="supply_id" name="supply_id" placeholder="supply_id">
                                        </div>
                                    </div>
                                    <div class="form-group required row">
                                        <label for="company_id" class="col-sm-3 control-label">Company</label>
                                        <div class="col-sm-9">
                                            <select name="company_id" id="company_id" class='form-control'>
                                            <?php echo getALLcompanies(); ?>
                                            </select>
                                        </div>
                                    </div>
                           

                                    <div class="form-group require row">
                                        <label for="product_id" class="col-sm-3 control-label">Product</label>
                                           <div class="col-sm-9" id="product">
                                                <select name='product_id' id='product_id' class='form-control'>
                                                    <option value=''>&mdash; Please Choose &mdash; </option>
                                                          <?php 
                                                            $prd = mysql_query("SELECT * FROM tbl_product ");
                                                            while($row = mysql_fetch_array($prd)){?>
                                                                <option value='<?php echo $row['product_id'];?>'><?php echo $row['product_name'];?></option>
                                                            <?php } ?>
                                                </select>
                                            </div>
                                    </div>
                                    <div class="form-group require row">
                                        <label for="qty" class="col-sm-3 control-label">Quantity</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="qty" name="qty" placeholder="Quantity" required>
                                        </div>
                                    </div>
                                    <div class="form-group require row">
                                        <label for="date_added" class="col-sm-3 control-label">Date</label>
                                        <div class="col-sm-9">
                                             <input type="date" name="date_added" id="date_added"placeholder="dd-mm-yyyy"  min="2020-01-01" max="<?php echo date("Y-m-d");?>"> 
                                            <!-- <input type="date" class="form-control" id="date_added" name="date_added" placeholder="" required> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" id='saveSupply' class="btn btn-primary">Save changes</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div><!--end modal-->
                </div><!--end card-body-->
            </div><!--end card-->
        </div><!--end col-->
    </div><!--end row-->

</div><!-- container -->
   <script src="../assets/pages/supply_table.js"></script> 
<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaCRM").addClass("active");
    $(".MetricaCRM_list").addClass("active");
    $("#link_supply").addClass("active");

  });

  function dealerTable(){
    $("#footable-3").DataTable().destroy();
    $("#footable-3").dataTable({
        "searching": false
    });

    $(".footable-editing").hide(); 
  }
  </script>