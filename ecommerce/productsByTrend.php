<style type="text/css">
    .text-none{
        color: #ccc !important;
    }
</style>
<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Ecommerce</a></li>
                        <li class="breadcrumb-item active">Products</li>
                    </ol>
                </div>
                <h4 class="page-title">Products</h4>
                <div class="col-sm-3">
                    <h6>Branch:</h6>
                    <select class="custom-select select2"  id="company_id" onchange="selectBranch()">
                       <!--  <option selected="">Select Branch</option> -->
                        <option value="-1">All Branch</option>

                        <?php 
                        $fetch_dealer = mysql_query("SELECT * FROM `tbl_company`");
                        while($row_dealer = mysql_fetch_array($fetch_dealer)){
                        echo '<option value="'.$row_dealer['company_id'].'">'.$row_dealer['company_name'].'</option>';
                        }?>
                    </select>
                </div>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
    
    <div class="row" id="product_data">     
        

    </div><!--end row-->
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaEcommerce").addClass("active");
    $(".MetricaEcommerce_list").addClass("active");
    $("#link_trend").addClass("active");

    selectBranch();
  });


  function selectBranch(){
    var company_id = $("#company_id").val();

    $.ajax({
        url:"../ajax/getTrendProductPerBranch.php",
        method:"POST",
        data:{
            company_id:company_id
        },success:function(data){
            $("#product_data").html(data);
        }
    });
  }


  function productdetails(id){
    var company_id = $("#company_id").val();
    window.location.replace("index.php?page=productDetail&id="+id+"&br_id="+company_id);
  }
</script>