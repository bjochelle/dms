<?php 
 $ref_num = $_GET['id'];
 $dealer_id = getData($ref_num,'tbl_transaction','dealer_id','ref_num');
$user_id = getData($ref_num,'tbl_transaction','user_id','ref_num');

?>
<style type="text/css">
    .project-dash-activity{
        min-height: 0px !important;
    }
    .slimscroll{
        height: 100px !important; 
    }
</style>
<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Transaction</a></li>
                        <li class="breadcrumb-item active">View Transaction</li>
                    </ol>
                </div>
                <h4 class="page-title">View Transaction</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div><!--end-row-->
    <!-- end page title end breadcrumb -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                  <div class="card-body">
                    <h4 class="header-title mt-0 mb-3"><span class="mdi mdi-map-marker"></span>Delivery Address</h4>
                    <h4 class="header-title mt-0 mb-3"></h4>
                    <div class="table-responsive">
                        <h6><?php echo getFullname($user_id)."<br>".getData($user_id,'tbl_user','contact_number','user_id')."<br>".getData($user_id,'tbl_user','address','user_id');?></h6>
                    </div><!--end re-table-->
                    <hr>
                      <h4 class="header-title mt-0 mb-3"><span class="fas fa-user-tie"></span> Dealer Information</h4>
                    <div class="table-responsive">
                        <h6><?php echo getFullname($dealer_id)."<br>".getData($dealer_id,'tbl_user','contact_number','user_id')."<br>".getData($dealer_id,'tbl_user','email','user_id');?></h6>
                    </div><!--end re-table-->
                    <hr>
                      <h4 class="header-title mt-0 mb-3"><span class="mdi mdi-truck-fast"></span> Shipping Information</h4>
                     <div class="slimscroll project-dash-activity" style="height: 100px !important;">
                        <div class="activity">
                            <?php
                            $count =1;
                            $fetch_track = mysql_query("SELECT * FROM tbl_track_transaction where ref_num='$ref_num'");
                            while($row_track=mysql_fetch_array($fetch_track)){

                                if($count == 1){
                                    echo '<i class="mdi mdi-cart-outline"></i>';
                                }else if($count ==2){
                                    echo '<i class="mdi mdi-truck-fast"></i>';
                                }else{
                                    echo '<i class="fas fa-box-open"></i>';
                                }?>
                            <div class="time-item">
                                <div class="item-info">
                                    <div class="text-muted text-right font-10"><?php echo date("F d, Y g:i a",strtotime($row_track['date_added']));?></div>
                                    <h5 class="my-0"><?php echo $row_track['module'];?></h5>
                                </div>
                            </div>  

                            <?php $count++;}?>
                                                        
                        </div><!--end activity-->
                    </div><!--end project-dash-activity-->
                   
                </div><!--end card-body-->
                 <hr>
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-3">Order Summary</h4>
                    <h4 class="header-title mt-0 mb-3">Ref # : <?php echo $ref_num;?></h4>
                    <div class="table-responsive shopping-cart">
                        <table class="table mb-0">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Quantity</th>                                                        
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                  <?php 
                               $fetch = mysql_query("SELECT * FROM tbl_transaction where user_id='$user_id' and ref_num='$ref_num' ORDER BY `tbl_transaction`.`date_added` DESC");
                                $subtotal = 0;
                                $shipping_fee = 0;
                                while($row = mysql_fetch_array($fetch)){
                                    $total = $row['qty']*getData($row['product_id'],'tbl_product','price','product_id');
                                    $subtotal+=$total;

                                    // if ($row['ship_opt']==0) {
                                    //     $shipping_fee = 100;
                                    // }else if ($row['ship_opt']==1){
                                    //     $shipping_fee = 150;
                                    // }
                                ?>

                                <tr>
                                    <td>
                                        <img src="../assets/images/products/<?php echo getData($row['product_id'],'tbl_product','filename','product_id');?>" alt="" height="52">
                                        <p class="d-inline-block align-middle mb-0 product-name"><?php echo substr(getData($row['product_id'],'tbl_product','product_name','product_id'),0,20)?></p> 
                                    </td>
                                    <td>
                                        <?php echo $row['qty'];?>
                                    </td>
                                    <td><?php echo number_format($total,2);?></td>                                                        
                                </tr>  
                                <?php }?>                                                  
                            </tbody>
                        </table>
                    </div><!--end re-table-->
                    <div class="total-payment">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td class="payment-title">Subtotal</td>
                                    <td>&#8369; <?php echo number_format($subtotal,2);?></td>
                                </tr>
                               <!--  <tr>
                                    <td class="payment-title">Shipping</td>
                                    <td>
                                        &#8369; <?php echo $shipping_fee;?>
                                    </td>
                                </tr> -->
                                <tr>
                                    <td class="payment-title">Total</td>
                                    <td class="text-dark"><strong> &#8369; <?php echo number_format($subtotal+$shipping_fee,2);?></strong></td>
                                </tr>
                            </tbody>
                        </table><!--end table-->
                    </div><!--end total-payment-->
                </div><!--end card-body-->
            </div><!--end card-->
        </div><!--end col-->
    </div><!--end row-->

</div><!-- container -->
<script type="text/javascript">
  $(document).ready(function(){
    // $(".MetricaOthers").addClass("active");
    // $(".MetricaOthers_list").addClass("active");
    // $("#link_pending").addClass("active");

    $("#checkoutForm").submit(function(e){
    e.preventDefault();

     $.ajax({
        url:"../ajax/finish_checkout.php",
        method:"POST",
        data:$(this).serialize(),
        success: function(data){

            if(data == 1){
                alert_notif('Done!','Successfully Checkout','success');
                setTimeout(function(){
                 window.location.replace("../ecommerce/index.php?page=pending");
                },1000)
            }else{
                failedQuery();
            }
          
        }
      });
    });
});
</script>