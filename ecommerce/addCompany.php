 <?php 
$array = array("January" => 01, "February" => 02, "March" => 03, "April" => 04, "May" => 05, "June" => 06, "July" => 07, "August" => 08, "September" => 09, "October" => 10, "November" => 11, "December" => 12);
 ?>
 <div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item"><a href="index.php?page=company">Company</a></li>
                        <li class="breadcrumb-item active">Add Company</li>
                    </ol>
                </div>
                <h4 class="page-title">Profile</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body  met-pro-bg">
                    <div class="met-profile">
                        <div class="row">
                            <div class="col-lg-4 align-self-center mb-3 mb-lg-0">
                                <div class="met-profile-main">
                                    <div class="met-profile-main-pic">
                                        <img src="../assets/images/users/user-4.jpg" alt="" class="rounded-circle">
                                        <span class="fro-profile_main-pic-change">
                                            <i class="fas fa-camera"></i>
                                        </span>
                                    </div>
                                    <div class="met-profile_user-detail">
                                        <h5 class="met-user-name">Branch Name</h5>
                                    </div>
                                </div>                                                
                            </div><!--end col-->
                            <div class="col-lg-4 ml-auto">
                                <ul class="list-unstyled personal-detail">
                                    <li class=""><i class="dripicons-phone mr-2 text-info font-18"></i> <b> phone </b> : +91 23456 78910</li>
                                    <li class="mt-2"><i class="dripicons-mail text-info font-18 mt-2 mr-2"></i> <b> Email </b> : dms@gmail.com</li>
                                    <li class="mt-2"><i class="dripicons-location text-info font-18 mt-2 mr-2"></i> <b>Location</b> : Philippines</li>
                                </ul>
                                <div class="button-list btn-social-icon">                                                
                                    <button type="button" class="btn btn-blue btn-round">
                                        <i class="fab fa-facebook-f"></i>
                                    </button>

                                    <button type="button" class="btn btn-secondary btn-round ml-2">
                                        <i class="fab fa-twitter"></i>
                                    </button>

                                    <button type="button" class="btn btn-pink btn-round  ml-2">
                                        <i class="fab fa-dribbble"></i>
                                    </button>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div><!--end f_profile-->                                                                                
                </div><!--end card-body-->
                <div class="card-body">
                    <ul class="nav nav-pills mb-0" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="general_detail_tab" data-toggle="pill" href="#general_detail">Branch Information</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="education_detail_tab" data-toggle="pill" href="#education_detail">Representative Information</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="settings_detail_tab" data-toggle="pill" href="#settings_detail">Authentication</a>
                        </li>
                    </ul>        
                </div><!--end card-body-->
            </div><!--end card-->
        </div><!--end col-->
    </div><!--end row-->
    <form method="POST" id="company_details">
    <div class="row">
        <div class="col-12">
            <div class="tab-content detail-list" id="pills-tabContent">
                <div class="tab-pane fade show active" id="general_detail">
                    <div class="row">
                        <div class="col-12">                        
                            <div class="card">
                                <div class="card-body">
                                     <span>Upload Branch Image</span>
                                    <input type="file" id="input-file-now-custom-1" name="company_profile" class="dropify" data-default-file="../assets/images/users/user-4.jpg"/>
                                    <br>
                                    <div class="">
                                        <div class="form-horizontal form-material mb-0">
                                            <div class="form-group">
                                                <input type="text" name='company_name' id='company_name' placeholder="Name" class="form-control">
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <input type="email" placeholder="Email" class="form-control" name="company_email" id="company_email" required>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="Contact Number" name="company_no" class="form-control" id='company_no'>
                                                </div>
                                               
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <textarea rows="5" name='company_address' id='company_address' placeholder="Blk. , Lot , Street , Brgy, City" class="form-control"></textarea>
                                                </div>
                                            </div>
                                           
                                            <div class="form-group">
                                               
                                                <button class="btn btn-primary btn-sm text-light px-4 mt-3 float-right mb-0 nav-link" id="education_detail_tab" data-toggle="pill" href="#education_detail">Go to Representative Information</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                            
                            </div>   
                        </div><!--end col-->
                    </div><!--end row-->                                             
                </div><!--end general detail-->

                <div class="tab-pane fade" id="education_detail">                                                
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">                                       
                                <div class="card-body"> 
                                     <span>Upload Profile Image</span>
                                    <input type="file" id="input-file-now-custom-1" name="filename_rep" class="dropify" data-default-file="../assets/images/users/user-4.jpg"/>
                                    <br>
                                    <div class="">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">Fullname: </span></div>
                                                    <input type="text" placeholder="First Name" class="form-control" name="rep_fname" id="rep_fname">
                                                     <input type="text" placeholder="Last Name" name="rep_lname" class="form-control" id='rep_lname'>
                                                      <input type="text" placeholder="Suffixes" name="rep_suffix" class="form-control" id='rep_suffix'>
                                                </div>
                                            </div>
                                            
                                           <div class="col-md-8" style="margin-top: 20px">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">Birthday: </span></div>
                                                    <select class="form-control" name="month" id="month">
                                                        <option value=""> &mdash; Choose Month &mdash; </option>
                                                        <?php 
                                                            foreach ($array as $key => $value) {
                                                            echo "<option value='$value'>$key</option>";
                                                            }
                                                        ?>
                                                    </select> 
                                                     <input type="text" placeholder="Day" name="day" class="form-control" id='day'>
                                                      <input type="text" placeholder="Year" name="year" class="form-control" id='year'>
                                                </div>
                                            </div>
                                            <div class="col-md-4" style="margin-top: 20px">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">Contact #: </span></div>
                                                     <input type="text" placeholder="Contact Number" name="rep_no" class="form-control" id='rep_no'>
                                                </div>
                                            </div>

                                            <div class="col-md-5" style="margin-top: 20px">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">Email: </span></div>
                                                     <input type="email" placeholder="Email" class="form-control" name="rep_email" id="rep_email">
                                                </div>
                                            </div>
                                            <div class="col-md-7" style="margin-top: 20px">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">Address: </span></div>
                                                     <textarea rows="2" style="resize: none;" name='rep_address' id='rep_address' placeholder="Blk. , Lot , Street , Brgy, City" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <button class="btn btn-primary btn-sm text-light px-4 mt-3 float-right mb-0 nav-link" id="settings_detail_tab" data-toggle="pill" href="#settings_detail">Go to Authentication</button>
                                            </div>
                                        </div>

                                </div>  <!--end card-body-->                                     
                            </div><!--end card-->
                        </div><!--end col-->

                    </div><!--end row-->  
                </div><!--end education detail-->
            </div>

                <div class="tab-pane fade" id="settings_detail">                                                
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">                                       
                                <div class="card-body"> 
                                    <div class="">
                                        <div class="form-horizontal form-material mb-0">
                                              <div class="form-group row">
                                                <div class="col-md-4">
                                                    <input type="text" placeholder="Username" class="form-control" name="username" id="username">
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="password" name='password' id='password' placeholder="password" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="password" name='repassword' id='repassword' placeholder="Re-password" class="form-control">
                                                </div>
                                            </div>
                                           
                                            <div class="form-group">
                                                <button type='submit' id='btn-save-company' class="btn btn-primary btn-sm text-light px-4 mt-3 float-right mb-0">Save</button>
                                            </div>
                                        </div>

                                </div>  <!--end card-body-->                                     
                            </div><!--end card-->
                        </div><!--end col-->

                    </div><!--end row-->  
                </div><!--end education detail-->


            </div><!--end tab-content--> 
            
        </div><!--end col-->
    </div><!--end row-->
    </form>
</div><!-- container -->
<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaCRM").addClass("active");
    $(".MetricaCRM_list").addClass("active");
    $("#link_company").addClass("active");

    $("#company_details").on('submit', function(e){
        e.preventDefault();
        $("#btn-save-company").prop("disabled", true);
        $("#btn-save-company").html("<span class='fa fa-spin fa-spinner'></span> Loading");
        $.ajax({
         url:"../ajax/add_company.php",
         type: "POST",
         data:  new FormData(this),
         beforeSend: function(){},
         contentType: false,
         cache: false,
         processData:false,
         success: function(data)
            {
                alert_notif("All Good!","Data was Successfully added new Company.","success");
                 $("#btn-save-company").prop("disabled", false);
                 $("#btn-save-company").html("Save");  
            },
             error: function() 
            {
                alert_notif("Aw Snap!","Unable to add new Company, Please Try again.","danger");
                
            }  
                  
       });
    });
  });
  </script>