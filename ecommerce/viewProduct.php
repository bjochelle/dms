<?php 

$id = $_GET['id'];

function getStarValue($val){
    $id = $_GET['id'];
    if($val == 0){
     $count = mysql_fetch_array(mysql_query("SELECT avg(star_value) FROM tbl_review where product_id='$id' "));
    }else if($val==-1){
     $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_review where product_id='$id' "));
    }else{
    $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_review where product_id='$id' and star_value='$val'"));
    }
    return $count[0];
}

function getStarValuePerc($val){
    $id = $_GET['id'];

    $count_all = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_review where product_id='$id' "));
 
    $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_review where product_id='$id' and star_value='$val'"));

    if($count_all[0]==0){
        $rating = 0;
    }else{
        $rating = ($count[0]/$count_all[0])*100;
    }

    return $rating;
}
?>
<style type="text/css">
    .text-none{
        color: #ccc !important;
    }
</style>
<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Ecommerce</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Product</a></li>
                        <li class="breadcrumb-item active">Product-Detail</li>
                    </ol>
                </div>
                <h4 class="page-title">Product Detail</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <img src="../assets/images/products/<?php echo getData($id,'tbl_product','filename','product_id');?>" alt="" class=" mx-auto  d-block" height="400" style="width: 100%;">                                           
                        </div><!--end col-->
                        <div class="col-lg-6 align-self-center">
                            <form id="addCart">
                            <div class="single-pro-detail">
                                <h3 class="pro-title"><?php echo getData($id,'tbl_product','product_name','product_id')?></h3>
                                <input type="hidden" name="product_id" value="<?php echo $id?>">
                                <ul class="list-inline mb-2 product-review">
                                     <?php 
                                        $start=1;
                                        $rating = number_format(getStarValue(0),1);
                                        while($start<=5){
                                            if($start<=$rating){
                                                $style='text-warning';
                                                $diff = 0 ;
                                                $half = "";
                                            }else{
                                                $diff = abs($rating - $start);
                                                $style='text-none';
                                                if($diff<1 && $diff>0){
                                                    $style='text-warning';
                                                    $half = "-half";
                                                }else{
                                                    $half = "";
                                                    $style='text-none';
                                                }
                                            }
                                        ?>
                                        <li class="list-inline-item"><i class="mdi mdi-star<?php echo $half;?> <?php echo $style;?>"></i></li>
                                        <?php $start++;}
                                        ?>
                                    <li class="list-inline-item"><?php echo number_format(getStarValue(0),1);?> (<?php echo getStarValue(-1);?> reviews)</li>
                                </ul>
                                <h2 class="pro-price">Php <?php echo getData($id,'tbl_product','price','product_id')?></h2>                                                 
                                <h6 class="text-muted font-13">Description :</h6> 
                                <ul class="list-unstyled pro-features border-0" style="height: 200px;overflow: auto;">
                                    <li><?php echo getData($id,'tbl_product','description','product_id')?></li>
                                </ul>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <a onclick="viewDemo()" style="cursor: help;">View Demo</a>
                                      
                                    </div>
                                </div>                     
                            </div>
                            </form>

                        </div><!--end col-->                                            
                    </div><!--end row-->
                </div><!--end card-body-->
            </div><!--end card-->
        </div><!--end col-->
    </div><!--end row-->

        <!-- modal -->
    <div class="modal about-modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-lg">
                <div class="modal-header">     
                        <h4 class="modal-title">Demo</h4>
                    </div> 
                    <div class="modal-body" id="modal_body">
                          <video src="../assets/images/product_demo/<?php echo getData($id,'tbl_product','video','product_id');?>" controls  style="width: 100%;" id="myVideo">    
                         
                    </div>
                </div>
            </div>
        </div>
    </div>

        
    </div><!--end row-->

</div><!-- container -->
<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaCRM").addClass("active");
    $(".MetricaCRM_list").addClass("active");
    $("#link_company_products").addClass("active");

     $(document).on("click",function() {

         $("#myVideo").get(0).pause(); 
    });

  });

   

  function viewDemo(){
    $("#myModal").modal('show');
  }

    $("#addCart").submit(function(e){
        e.preventDefault();
        $.ajax({
            url:"../ajax/addTransaction.php",
            method:"POST",
            data:$(this).serialize(),
            success: function(data){
                if(data == 1){
                    success();
                }else{
                    failedQuery();
                }
            
            }
        });
    });

</script>