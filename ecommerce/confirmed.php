 <!-- Page Content-->
<div class="page-content">

    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="float-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Transactions</a></li>
                            <li class="breadcrumb-item active">Confirmed</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Confirmed</h4>
                </div><!--end page-title-box-->
            </div><!--end col-->
        </div><!--end row-->
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">Confirmed Transactions</h4>

                        <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                            <tr>
                                <th>Ref #</th>
                                <th>Total Price</th>
                                <th>Date Added</th>
                                <th>Receipt</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->
    </div><!-- container -->
</div>
<!-- end page content -->
<form onsubmit="return false" method='POST' id='receipt-add' enctype="multipart/form-data">
<div class="modal fade" id="receipt_modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title"><span class="fa fa-upload"></span> Upload Receipt </h4>

          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>            
      </div>
      <div class="modal-body">
          
            <div class='col-12'>
                <div class="form-group row">
                    <label for="example-number-input" class="col-sm-2 col-form-label text-right">Receipt Image</label>
                    <div class="col-sm-10">
                        <input type="file" id="input-file-now-custom-1" name="receipt_image" class="dropify" data-default-file="../assets/images/users/user-4.jpg"/>
                        <input type="hidden" id="trans_id" id='' name="trans_id">
                    </div>
                </div>
            </div>
          
      </div>
      <div class="modal-footer">
        <button type="submit" id='uploadReceiptImage' class="btn btn-primary"><span class="fa fa-upload"></span> Upload</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
</form>
<div class="modal fade" id="view_receipt_modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title"><span class="fa fa-eye"></span> View Receipt </h4>

          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>            
      </div>
      <div class="modal-body">
          <div class='col-12'>
              <img id='receiptImg' style="height: 100%;width: 100%;object-fit: contain;">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $(".MetricaOthers").addClass("active");
    $(".MetricaOthers_list").addClass("active");
    $("#link_confirmed").addClass("active");
      getData();


      $("#receipt-add").on('submit', function(e){
        e.preventDefault();
        $("#uploadReceiptImage").prop("disabled", true);
        $("#uploadReceiptImage").html("<span class='fa fa-spin fa-spinner'></span> Loading");
        $.ajax({
         url:"../ajax/upload_receipt.php",
         type: "POST",
         data:  new FormData(this),
         beforeSend: function(){},
         contentType: false,
         cache: false,
         processData:false,
         timeout: 600000,
         success: function(data)
            {
              $("#receipt_modal").modal('hide');
                if(data > 0){
                   alert_notif("All Good!","Receipt was successfully uploaded.","success");
                }else{
                  alert_notif("Aw Snap!","Unable to Upload receipt, Please Try again.","danger");
                }

                getData();
                
            },
             error: function() 
            {
                 alert_notif("Aw Snap!","Unable to add new Company, Please Try again.","danger");
            }        
       });
    });
  });
function viewImg(imgPath){
  $("#view_receipt_modal").modal();
  $("#receiptImg").attr("src","../assets/images/receipts/"+imgPath+"");
}
function uploadReceipt(id){
    $("#receipt_modal").modal();
    $("#trans_id").val(id);
  }
function viewDetails(id){
    var ref_num = $("#view"+id).val();
    window.location.replace("index.php?page=viewTransaction&id="+ref_num);
}

function confirm(id){
    var ref_num = $("#view"+id).val();
     $.ajax({
        url:"../ajax/finishTransaction.php",
        method:"POST",
        data:{ref_num:ref_num
        },
        success: function(data){
           if(data == 1){
                alert_notif("All Good!","Order was successfully received.","success");
                getData();
            }else{
                failedQuery();
            }
        }
    });
}


    function getData(){
      var table = $('#datatable').DataTable();
      table.destroy();
      var status = 'C';
      $("#datatable").dataTable({
        "processing":true,
        "ajax":{
          "method":"POST",
          "url":"../ajax/datatables/dt_trans.php",
          "data":{
            status:status
          },
          "dataSrc":"data"
        },
        "columns":[
          {
            "data":"ref_num"
          },
          {
            "data":"total_price"
          },
          {
            "data":"date"
          },  
          {
            "data":"receipt"
          },         
          {
            "mRender": function(data,type,row){
              if(row.finish == 1){
                return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Details' value='"+row.ref_num+ "' onclick='viewDetails("+row.id+")' id='view"+row.id+"'><span class='fa fa-eye'></span></button><button class='btn btn-success btn-sm' data-toggle='tooltip' title='Received' value='"+row.ref_num+ "' onclick='confirm("+row.id+")' id='confirm"+row.id+"'><span class='fas fa-box-open'></span></button></center>";
              }else{
                return "<center><button class='btn btn-primary btn-sm' data-toggle='tooltip' title='View Details' value='"+row.ref_num+ "' onclick='viewDetails("+row.id+")' id='view"+row.id+"'><span class='fa fa-eye'></span></button><button class='btn btn-success btn-sm' data-toggle='tooltip' title='Upload Receipt' value='"+row.ref_num+ "' onclick='uploadReceipt("+row.id+")' id='uploadReceipt"+row.id+"'><span class='fa fa-receipt'></span></button></center>";

              }
            }
          }
        ]
      });
    }

</script>