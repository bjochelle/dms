<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>AG-Mech Systems Corp. | John Deere Tractors and Harvesters | Philippines</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- App favicon -->
<link rel="icon" href="web/images/logo.png" type="image/icon type">
<meta charset="utf-8">
<meta name="keywords" content=""s />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link href="web/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstrap-css -->
<!-- css -->
<link rel="stylesheet" href="web/css/style.css" type="text/css" media="all" />
<link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
<link href="web/css/jQuery.lightninBox.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="web/css/flexslider.css" type="text/css" media="screen" property="" />
<!-- animation -->
<link href="web/css/aos.css" rel="stylesheet" type="text/css" media="all" /><!-- //animation effects-css-->
<!-- //animation -->
<!--// css -->
<!-- font-awesome icons -->
<link href="web/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Oswald:400,500,600,700" rel="stylesheet">
<!-- //font -->
<script src="web/js/jquery-2.2.3.min.js"></script>
<script src="web/js/bootstrap.js"></script>

<style type="text/css">
	.img-fit
	{
		height: 400px;object-fit: cover;
	}
</style>
</head>
<body>
	<!-- w3-banner -->
	<div class="w3-banner jarallax">
		<div class="wthree-different-dot">
			<div class="head">
				<div class="container">
					<div class="navbar-top">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
							  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							  </button>
								 <div class="navbar-brand logo ">
								 	<img src="web/images/logo.png" alt=" " class="img-responsive">
									<!-- <h1><a href="index.html">New Party</a></h1> -->
								</div>

							</div>

							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							 <ul class="nav navbar-nav link-effect-4">
								<li class="active first-list"><a href="index.html">Home</a></li>
								<li><a href="#about" class="scroll">About Us</a></li>
								<li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span data-letters="Pages">Product Lines</span><span class="caret"></span></a>
									<ul class="dropdown-menu"> 
										<li><a href="#client" class="scroll">Tractors</a></li>
										<li><a href="#gallery" class="scroll">Agricultural Implements</a></li>
									</ul>
								</li>   
								<li><a href="#pricing" class="scroll">Services</a></li>
								<li><a href="#services" class="scroll">Affiliate Companies</a></li>
								
								
								
								<li><a href="#book" class="scroll">Contact Us/Login </a></li>
								
							  </ul>
							</div><!-- /.navbar-collapse -->
						</div>
				</div>
			</div>
			<!-- banner -->
			<div class="banner">
				<div class="container">
					<div class="slider">
						
						<script src="web/js/responsiveslides.min.js"></script>
						<script>
								// You can also use "$(window).load(function() {"
								$(function () {
								// Slideshow 4
									$("#slider3").responsiveSlides({
										auto: true,
										pager: true,
										nav: true,
										speed: 500,
										namespace: "callbacks",
										before: function () {
											$('.events').append("<li>before event fired.</li>");
										},
										after: function () {
											$('.events').append("<li>after event fired.</li>");
										}
									 });				
								});
						</script>
						<div  id="top" class="callbacks_container-wrap">
							<ul class="rslides" id="slider3">
								<li>
									<div class="slider-info" data-aos="fade-left">
									<h6>AG-MECH Systems Corporation</h6>
										<h3>John Deere</h3>
										<p >is the appointed distributor of John Deere tractors and implements line in the Philippines.</p>
									</div>
								</li>
								<li>
									<div class="slider-info" data-aos="fade-left">
									<h6>AG-MECH Systems Corporation</h6>
										<h3>SERVICES</h3>
										<p>Get the quality and features you demand, at a price you wouldn't expect...</p>
									</div>
								</li>
								<li>
									<div class="slider-info" data-aos="fade-left">
									<h6>AG-MECH Systems Corporation</h6>
										<h3>VALUES</h3>
										<p>We uphold the values of professionalism,integrity,commitment,excellence and realibity</p>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- //banner -->
		</div>
	</div>
	<!-- //w3-banner -->
	<!-- modal -->
	<div class="modal about-modal fade" id="myModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header"> 
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						<h4 class="modal-title">Register</h4>
					</div> 
					<div class="modal-body">
					<div class="agileits-w3layouts-info">
						 <form id="regForm">
        
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <div class="input-group" style="width: 100%;">        
                                        <input type="text" class="form-control" id="username" name="username" placeholder="Enter username">
                                    </div>                                    
                                </div><!--end form-group--> 
                                <div class="form-group">
                                    <label for="userpassword">Password</label>                                            
                                    <div class="input-group" style="width: 100%;">                                                       
                                        <input type="password" class="form-control" id="userpassword" name="password" placeholder="Enter password">
                                    </div>                               
                                </div><!--end form-group--> 

                                <div class="form-group">
                                    <label for="conf_password">Confirm Password</label>                                            
                                    <div class="input-group" style="width: 100%;">                                                       
                                        <input type="password" class="form-control" id="conf_password" placeholder="Enter Confirm Password">
                                    </div>
                                </div><!--end form-group--> 
    
                                <div class="form-group">
                                    <div class="col-12">
                                        <button class="btn btn-primary btn-round btn-block waves-effect waves-light" type="submit">Register <i class="fas fa-sign-in-alt ml-1"></i></button>
                                    </div><!--end col--> 
                                </div> <!--end form-group-->                           
                            </form><!--end form-->
                         
                        <p class="animated" id="notif_p" style="text-align: center;"> <span id="notif" > </span></p>
                           <br>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //modal -->
	<div class="banner-bottom" id="about">
		<div class="container">
			<h3 class="heading-agileinfo" data-aos="zoom-in">About Us<span>Mission Vision & Values</span></h3>
			<div class="w3ls_banner_bottom_grids">
				<div class="col-md-4 agileits_services_grid" data-aos="fade-right">
					
					<h3>VISION</h3>
					<p>We aim to be the country’s leader in providing agricultural mechanization systems and related products and services by becoming a world-class equipment distribution and service company with a reputation for expertise, reliability and quality by maximizing our combined strengths through solid teamwork.<br><br>
					The ultimate satisfaction of our customers is the driving purpose of our existence.<br><br>

					We aspire for excellence in everything we do by adhering to the highest standards of professionalism, by making integrity the foundation of all our relationships, and by focusing on the unlimited potential of our people.
					We are a responsible partner on the road to progress and development, essential to our country’s collective hope for a better life.</p>
				
				</div>
				<div class="col-md-4 agileits_services_grid" data-aos="fade-up">
					
					<h3>MISSION</h3>
					<p>Our mission is to understand the business of our customers and prospective customers, assess their needs and provide creative, responsive, timely, cost effective products by drawing upon our extensive experience and resources.  We are a company primarily engaged in agricultural equipment distribution and in providing allied products and services in the Philippines.<br><br>
						We are committed to:<br>

						<br><strong>1. Providing quality products and services to our customers;</strong>
						<br><strong>2. Creating high-quality brand products that will gain the trust of our clients;</strong>
						<br><strong>3. Building stronger relationships with our suppliers;</strong>
						<br><strong>4. Upholding the values of professionalism, integrity, commitment, excellence and reliability;</strong>
						<br><strong>5. Extending meaningful employment and a better quality of life to our employees;</strong>
						<br><strong>6. Delivering reasonable returns to our shareholders; and</strong>
						<br><strong>7. Servicing the community at large</strong>

					</p>
				
				</div>
				<div class="col-md-4 agileits_services_grid" data-aos="fade-left">
				
					<h3>VALUES</h3>
					<p>We are an organization committed to:
						<br><strong>1. Pursuing EXCELLENCE in everything we say or do;</strong>
						<br><strong>2. Embracing CHANGE and INNOVATION in our quest for continuous improvement; and</strong>
						<br><strong>3. Instituting INTEGRITY as the bedrock of employees and corporate character</strong></p>
			
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- services -->
<div class="services jarallax" id="services">
		<h3 class="heading-agileinfo" data-aos="zoom-in">Services<span class="thr">Events Is A Professionally Managed Event</span></h3>
	<div class="container">
		
			<div class="col-md-4 ser-1" data-aos="fade-right">
				<div class="grid1">
					<img src="web/images/Alpha.png" alt=" " class="img-responsive">
					<h4>Alpha Machinery & Engineering Corporation</h4>
					<p>
						G/F CLF 1 Bldg., 1167 Chino Roces Avenue , Makati City
						<br>Tel No.: (632) 896-5556 to 59 </p>
				</div>
			</div>
			<div class="col-md-4 ser-1" data-aos="fade-down">
				<div class="grid1">
					<img src="web/images/AlliedSales.png" alt=" " class="img-responsive">
					<h4>Allied Sales Corporation</h4>
					<p>CLF 1 Bldg., 1167 Chino Roces Avenue , Makati City <br> Tel No.: (632) 897-0696</p>
				</div>
			</div>
			<div class="col-md-4 ser-1" data-aos="fade-left">
				<div class="grid1">
					<img src="web/images/CLFGIncLOGO.png" alt=" " class="img-responsive">
					<h4>CLFG Inc.</h4>
					<p>CLF 1 Bldg., 1167 Chino Roces Avenue , Makati City <br>Tel No.: (032) 239-5401</p>
				</div>
			</div>
		
			<div class="col-md-4 ser-1" data-aos="fade-right">
				<div class="grid1">
					<img src="web/images/folloso.png" alt=" " class="img-responsive">
					<h4>Follosco Manufacturing & Industrial Corporation</h4>
					<p>Sampaguita Avenue , United Paranaque Industrial Subdivision IV, Km. 19 West Service Road, Paranaque City
						<br>Tel No.: (632) 823-3714 to 15
						<br> Fax No.: (632) 824-4265</p>
				</div>
			</div>
			<div class="col-md-4 ser-1" data-aos="fade-up">
				<div class="grid1">
					<img src="web/images/Hydrotechlogo.png" alt=" " class="img-responsive">
					<h4>Hydrotech Systems & Resources Corporation</h4>
					<p>Sampaguita Avenue , United Paranaque Industrial Subdivision IV, Km. 19 West Service Road, Paranaque City
						<br>Tel No.: (632) 823-3714
						<br>Fax No.: (632) 824-4265 </p>
				</div>
			</div>
			<div class="col-md-4 ser-1" data-aos="fade-left">
				<div class="grid1">
					<img src="web/images/problends.png" alt=" " class="img-responsive">
					<h4>Problends Food CorporationS</h4>
					<p>Sampaguita Avenue , United Paranaque Industrial Subdivision IV, Km. 19 West Service Road, Paranaque City
						<br>Tel No.: (632) 823-3714
						<br>Fax No.: (632) 824-4265 </p>
				</div>
			</div>
			<div class="clearfix"></div>
		
	</div>
</div>
<!-- //services -->
	
	<!-- Portfolio section -->
	<section class="portfolio-agileinfo" id="gallery">
		<div class="container">
			<h3 class="heading-agileinfo" data-aos="zoom-in">Agricultural Implements</h3>
				<div class="gallery-grids">
					<div class="tab_img">
						<div class="col-md-3 col-sm-6 col-xs-6 portfolio-grids" data-aos="zoom-in">
								<h4>Disc Plow</h4>
							<a href="web/images/disk_plow.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="web/images/disk_plow.jpg" class="img-responsive" alt="w3ls" />
								<div class="b-wrapper">
									<i class="fa fa-search-plus" aria-hidden="true"></i>

									
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 portfolio-grids" data-aos="zoom-in">
								<h4>Trailing Disc Harrow</h4>
							<a href="web/images/disk_harrow.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="web/images/disk_harrow.jpg" class="img-responsive" alt="w3ls"/>
								<div class="b-wrapper">
									<i class="fa fa-search-plus" aria-hidden="true"></i>
									
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 portfolio-grids" data-aos="zoom-in">
							<h4>Mounted Disc Harrow</h4>
							<a href="web/images/mounted_disk_harrow.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="web/images/mounted_disk_harrow.jpg" class="img-responsive" alt="w3ls"/>
								<div class="b-wrapper">
									<i class="fa fa-search-plus" aria-hidden="true"></i>
									
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 portfolio-grids" data-aos="zoom-in">
							<h4>Moldboard Plow</h4>
							<a href="web/images/moldboard.png" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="web/images/moldboard.png" class="img-responsive" alt="w3ls"/>
								<div class="b-wrapper">
									<i class="fa fa-search-plus" aria-hidden="true"></i>
									
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 portfolio-grids" data-aos="zoom-in">
							<h4>Rotavator</h4>
							<a href="web/images/rotavator.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="web/images/rotavator.jpg" class="img-responsive" alt="w3ls"/>
								<div class="b-wrapper">
									<i class="fa fa-search-plus" aria-hidden="true"></i>
									
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 portfolio-grids" data-aos="zoom-in">
							<h4>Furrower</h4>
							<a href="web/images/furrower.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="web/images/furrower.jpg" class="img-responsive" alt="w3ls"/>
								<div class="b-wrapper">
									<i class="fa fa-search-plus" aria-hidden="true"></i>
									
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 portfolio-grids" data-aos="zoom-in">
							<h4>Cultivator</h4>
							<a href="web/images/cultivator.webp" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="web/images/cultivator.webp" class="img-responsive" alt="w3ls"/>
								<div class="b-wrapper">
									<i class="fa fa-search-plus" aria-hidden="true"></i>
									
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 portfolio-grids" data-aos="zoom-in">
							<h4>Loader</h4>
							<a href="web/images/loader.webp" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="web/images/loader.webp" class="img-responsive" alt="w3ls"/>
								<div class="b-wrapper">
									<i class="fa fa-search-plus" aria-hidden="true"></i>
									
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 portfolio-grids" data-aos="zoom-in">
							<h4>Grader</h4>
							<a href="web/images/Grader.png" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="web/images/Grader.png" class="img-responsive" alt="w3ls"/>
								<div class="b-wrapper">
									<i class="fa fa-search-plus" aria-hidden="true"></i>
									
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 portfolio-grids" data-aos="zoom-in">
							<h4>Trailer</h4>
							<a href="web/images/trailer.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="web/images/trailer.jpg" class="img-responsive" alt="w3ls"/>
								<div class="b-wrapper">
									<i class="fa fa-search-plus" aria-hidden="true"></i>
									
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 portfolio-grids" data-aos="zoom-in">
							<h4>Rotary Slasher</h4>
							<a href="web/images/rotary_slasher.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="web/images/rotary_slasher.jpg" class="img-responsive" alt="w3ls"/>
								<div class="b-wrapper">
									<i class="fa fa-search-plus" aria-hidden="true"></i>
									
								</div>
							</a>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6 portfolio-grids" data-aos="zoom-in">
							<h4>Cannon Sprayer</h4>
							<a href="web/images/canon_sprayer.webp" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
								<img src="web/images/canon_sprayer.webp" class="img-responsive" alt="w3ls"/>
								<div class="b-wrapper">
									<i class="fa fa-search-plus" aria-hidden="true"></i>
									
								</div>
							</a>
						</div>
						
						<div class="clearfix"> </div>
					</div>
				
			</div>
		</div>
</section>
<!-- /Portfolio section -->
	<!-- Clients -->
	<div class="clients-main jarallax" id="client">
		<div class="container">
			<!-- Owl-Carousel -->
			<h3 class="heading-agileinfo" data-aos="zoom-in">Tractors</h3>
			<div class="cli-ent" data-aos="fade-down">
				<section class="slider">
						<div class="flexslider">
							<ul class="slides">
								<li>
									<div class="item g1">
										<div class="agile-dish-caption">
										<img class="lazyOwl" src="web/images/5715.png" alt="" />
											<h4>5715 2WD & MFWD TRACTOR</h4>
										</div>
										<div class="clearfix"></div>
										<p class="para-w3-agile">Perfect for property owners and part-time producers, the 5715 2WD & MFWD Tractors tractors offer serious utlity tractor perfromance in a value priced package.</p>
									</div>
								</li>
								<li>
									<div class="item g1">
										<div class="agile-dish-caption">
										<img class="lazyOwl" src="web/images/5610.png" alt="" />
											<h4>5610 74 HP TRACTOR</h4>
										</div>
										
										<div class="clearfix"></div>
										<p class="para-w3-agile">John Deere's modern Synchromesh type gear box facilitates effortless, noiseless gear change with higher transmission efficiency.Pressurised lubrication of top shaft and gears reduces wear, resulting in enhanced life.</p>
									</div>
								</li>
								<li>
									<div class="item g1">
										<div class="agile-dish-caption">
										<img class="lazyOwl" src="web/images/6525.png" alt="" />
											<h4>6525 120 HP TRACTOR</h4>
										</div>
										
										<div class="clearfix"></div>
										<p class="para-w3-agile">The fullframe design is more rigid and distributes implement weight evenly across the entire frame. Less stress is placed on the drivetrain and component. And it allows for perfect integration of John Deere loaders. In fact, this deisgn is used in all 6000 series Tractors for unmatched reliability and strength.</p>
									</div>
								</li>
							</ul>
						</div>
				</section>
			</div>
			<!--// Owl-Carousel -->
		</div>
	</div>
	<!--// Clients -->
	<div class="pricing" id="pricing">
		<div class="container">
			<h3 class="heading-agileinfo" data-aos="zoom-in">Services<span>Agricultural Services</span></h3>
			<div class="w3ls_banner_bottom_grids">
				<div class="col-md-3 agileits_services_grid" data-aos="fade-right">
					<div class="w3_agile_services_grid1">
						<img src="web/images/land_prep.jpg" alt=" " class="img-responsive" style="height: 180px;object-fit: cover;">
						<div class="w3_blur"></div>
					</div>
					<div class="pr-ta">
						<h3>Land Preparation</h3>
					</div>
				</div>
				<div class="col-md-3 agileits_services_grid" data-aos="fade-up">
					<div class="w3_agile_services_grid1">
						<img src="web/images/crop_main.jpeg" alt=" " class="img-responsive" style="height: 180px;object-fit: cover;">
						<div class="w3_blur"></div>
					</div>
					<div class="pr-ta">
						<h3 style="font-size: 20px;">Crop Maintenance</h3>
					</div>
				</div>
				<div class="col-md-3 agileits_services_grid" data-aos="fade-left">
					<div class="w3_agile_services_grid1">
						<img src="web/images/grass_cutting.jpg" alt=" " class="img-responsive" style="height: 180px;object-fit: cover;">
						<div class="w3_blur"></div>
					</div>
					<div class="pr-ta">
						<h3>Grass-cutting</h3>
					</div>
				</div>
				<div class="col-md-3 agileits_services_grid" data-aos="fade-left">
					<div class="w3_agile_services_grid1">
						<img src="web/images/spraying.jpg" alt=" " class="img-responsive" style="height: 180px;object-fit: cover;">
						<div class="w3_blur"></div>
					</div>
					<div class="pr-ta">
						<h3>Spraying</h3>
						
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- register -->
	<div class="register-sec-w3l jarallax row" id="book">
		<!-- <div class="container col-md-6">
		<h3 class="heading-agileinfo" data-aos="zoom-in">Inquiry Form<span class="thr">Let us know your offer </span></h3>
			<div class="book-appointment" data-aos="zoom-in">
				<form action="#" method="post">
					<div class="gaps">
						<p></p>
						<input type="text" name="Name" placeholder="Name" required="" />
					</div>
					<div class="gaps">
						<p></p>
						<input type="email" name="email" placeholder="Email" required="" />
					</div>
					<div class="gaps">
						<p></p>
						<input type="text" name="Phone Number" placeholder="Phone Number" required="" />
					</div>
					<div class="gaps">
						<textarea name="Message" placeholder="Message..." required=""></textarea>
					</div>
					<input type="submit" value="Inquire Now">
				</form>
				
			</div>
		</div> -->
		<div class="container col-md-12">
		<h3 class="heading-agileinfo" data-aos="zoom-in">Login Form<span class="thr">Manage your transaction easily</span></h3>
			<div class="book-appointment" data-aos="zoom-in">
				<form method="post" id="loginForm">
					<div class="gaps">
						<p></p>
						<input type="text" name="username" placeholder="Username" required="" autocomplete="off" />
					</div>
					<div class="gaps">
						<p></p>
						<input type="password" name="password" placeholder="Password" required="" autocomplete="off" />
					</div>
					<input type="submit" value="Login">
				</form>
				<br>
				 <h5 class="thr" style="color: white">Don't have an account? <a href="#" data-toggle="modal" data-target="#myModal"> Free Register</a></h5> 
				<br>
				<p class="animated" id="notif_login" style="text-align: center;"> <span id="notif_login" > </span></p>
			</div>
		</div>
	</div>
	<!-- //register -->
<!-- Team section -->
<section class="team" id="team">
	<div class="container">
			<h3 class="heading-agileinfo" data-aos="zoom-in">OUR TEAM</h3>
		<div class="team-grid-top">
			<div class="col-md-4 team1" data-aos="fade-right">
				<div class="inner-team1">
				<img src="web/images/regen.jpg" class="img-fit" alt="" />
				<h3>Regen Songcog</h3>
				<h4 style="color: #fdda04;">Project Manager</h4>
			
				</div>
			</div>
			<div class="col-md-4 team1" data-aos="fade-down">
				<div class="inner-team1">
				<img src="web/images/mikee.jpg" class="img-fit" alt="" />
				<h3>Mikee Jarina</h3>
				<h4 style="color: #fdda04;">Programmer</h4>
				
				</div>
			</div>
			<div class="col-md-4 team1" data-aos="fade-up">
				<div class="inner-team1">
				<img src="web/images/melliza.jpg" class="img-fit" alt="" />
				<h3>Khiezzynet Melliza</h3>
				<h4 style="color: #fdda04;">Researcher</h4>
					
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
		
	</div>
</section>
<!-- //Team section -->
<!-- Stats -->
	<div class="stats news-w3layouts jarallax"> 
		<div class="container">    
			<div class="stats-agileinfo agileits-w3layouts">
				<div class="col-sm-3 col-xs-6 stats-grid" data-aos="fade-right">
					<div class="stats-img">
						<i class="fa fa-trophy" aria-hidden="true"></i>
					</div>
					<p>Total Sales</p>
					<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='157000' data-delay='.5' data-increment="100">157000</div>
				</div>
				<div class="col-sm-3 col-xs-6 stats-grid" data-aos="fade-up">
					<div class="stats-img w3-agileits">
						<i class="fa fa-calendar-check-o" aria-hidden="true"></i>
					</div>
					<p>Deals last month</p>
					<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='850' data-delay='8' data-increment="1">850</div>
				</div>
				<div class="col-sm-3 col-xs-6 stats-grid" data-aos="fade-down">
					<div class="stats-img w3-agileits">
						<i class="fa fa-briefcase" aria-hidden="true"></i>
					</div>	
					<p>visitors last month</p> 
					<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='5000' data-delay='.5' data-increment="10">5000</div>
				</div>
				<div class="col-sm-3 col-xs-6 stats-grid" data-aos="fade-left">
					<div class="stats-img w3-agileits">
						<i class="fa fa-users" aria-hidden="true"></i>
					</div>
					<p>Customer</p>
					<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='110' data-delay='8' data-increment="1">110</div>
				</div>
				<div class="clearfix"></div>
			</div> 
			<!-- Progressive-Effects-Animation-JavaScript -->  
			<script type="text/javascript" src="web/js/numscroller-1.0.js"></script>
			<!-- //Progressive-Effects-Animation-JavaScript -->
		</div>
	</div>
	<!-- //Stats -->


	<!-- footer -->
	<div class="footer">
	<div class="container">
		
		<div class="f-bg-w3l">
			<h2 style="    font-size: 1.5em;
						    color: #fff;
						    position: relative;
						    margin-bottom: 1.5em;
						    letter-spacing: 2px;">Contact Information</h2>
		<div class="col-md-4 w3layouts_footer_grid" data-aos="fade-right">
					<h2>MAIN OFFICE</h2>
					<ul class="con_inner_text">
							<li><span class="fa fa-map-marker" aria-hidden="true"></span> Chiu warehouse,Purok Sabis Brgy.Villamonte,<label>  Bacolod City.</label></li>
							<li><span class="fa fa-phone" aria-hidden="true"></span> Tel No.:(034) 702-8065</li>
						</ul>
				</div>
				<div class="col-md-4 w3layouts_footer_grid" data-aos="fade-down">
					<h2>DAVAO CITY</h2>
					    <ul class="con_inner_text">
							<li><span class="fa fa-map-marker" aria-hidden="true"></span>Principe Bldg. JP Laurel Ave., Bajada <label>  Davao City.</label></li>
							<li><span class="fa fa-fax" aria-hidden="true"></span> Fax No.:  (6382) 2276931</li>
						</ul>
					<br>
					<h2>MANDAUE CITY, CEBU</h2>
					    <ul class="con_inner_text">
							<li><span class="fa fa-map-marker" aria-hidden="true"></span>Unit G-07, North Road Plaza Bldg., Labogon Road, Mandaue City, <label> Cebu.</label></li>
							<li><span class="fa fa-fax" aria-hidden="true"></span>  (6332) 239-5401</li>
						</ul>
				</div>
				<div class="col-md-4 w3layouts_footer_grid" data-aos="fade-left">
					<h3>URDANETA CITY</h3>
					 <ul class="con_inner_text">
							<li><span class="fa fa-map-marker" aria-hidden="true"></span>Mc Arthur Highway, Nancayasan, Urdaneta  City, <label> Cebu.</label></li>
							<li><span class="fa fa-fax" aria-hidden="true"></span> (6375) 656-2143</li>
					</ul>
					<h3>MAKATI CITY </h3>
						<ul class="con_inner_text">
							<li><span class="fa fa-map-marker" aria-hidden="true"></span>G/F, CLF 1 Bldg., 1167 Chino Roces Avenue <label>  Makati City.</label></li>
							<li><span class="fa fa-phone" aria-hidden="true"></span> Tel No.: (632) 890-7205 / 896-5556 to 59</li>
							<li><span class="fa fa-fax" aria-hidden="true"></span> Fax No.: (632) 898-3284</li>
							<li><span class="fa fa-phone" aria-hidden="true"></span>Cel No.:  0918-9670616 </li>
						</ul>
					
				</div>
				<div class="clearfix"> </div>
			</div>
			</div>
			<p class="copyright" data-aos="zoom-in">© 2020 AG-MECH. All Rights Reserved</p>
	</div>
	<!-- //footer -->

<!-- js for portfolio lightbox -->
<script src="web/js/jQuery.lightninBox.js"></script>
<script type="text/javascript">
	$(".lightninBox").lightninBox();
</script>
<!-- /js for portfolio lightbox -->
<!-- flexSlider -->
				<script defer src="web/js/jquery.flexslider.js"></script>
				<script type="text/javascript">
					$(window).load(function(){
					  $('.flexslider').flexslider({
						animation: "slide",
						start: function(slider){
						  $('body').removeClass('loading');
						}
					  });
					});
				</script>
<!-- //flexSlider -->

<script type="text/javascript" src="web/js/move-top.js"></script>
	<script type="text/javascript" src="web/js/easing.js"></script>

	<script src="web/js/jarallax.js"></script>
	<script src="web/js/SmoothScroll.min.js"></script>
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed: 0.5,
			imgWidth: 1366,
			imgHeight: 768
		})
	</script><!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
					

		$("#regForm").submit(function(e){
		    e.preventDefault();
		    $("#notif_p").removeClass("shake");
		    $("#notif_p").removeClass("fadeIn");

		    var userpassword = $("#userpassword").val();
		    var conf_password = $("#conf_password").val();

		    	if(userpassword == conf_password){
			        $.ajax({
			        url:"ajax/register.php",
			        method:"POST",
			        data:$(this).serialize(),
			        success: function(data){
			        	alert(data)

			            if(data == 1){
			                $("#notif_p").toggleClass("fadeIn");
			                $("#notif").html("<span class=' alert alert-success'>Successfully Registered</span>");

			                setTimeout(function(){
			                	$("#myModal").modal('hide');
			                     // window.location.replace("index.php");
			                },2000);
			            }else if(data == 2){
			                 $("#notif_p").toggleClass("shake");
			                $("#notif").html("<span class='alert alert-danger'>Username is already exist</span>");
			            }else{
			                $("#notif_p").toggleClass("shake");
			                $("#notif").html("<span class='alert alert-danger'>Please try again later.</span>");
			            }

			        }
			      });
			    }else{
			        $("#notif").html("<span class='alert alert-danger'>Password didn't match to confirm password.</span>");
			    }
		     
		    });

		$("#loginForm").submit(function(e){
		    e.preventDefault();
		   
		     $.ajax({
		        url:"ajax/login_user.php",
		        method:"POST",
		        data:$(this).serialize(),
		        success: function(data){
		          if(data!=0){
		            // $("#notif_p").toggleClass("fadeIn");
		            $("#notif_login").html("<span class=' alert alert-success'>Successfully Login</span>");
		            setTimeout(function(){
		                 window.location.replace("ecommerce/index.php?page="+data);
		            },1000);
		         }else{
		            // $("#notif_p").toggleClass("shake");
		             $("#notif_login").html("<span class='alert alert-danger'>Incorrect Password or Username</span>");
		          }
		        }
		      });
		    });

			$().UItoTop({ easingType: 'easeOutQuart' });});
	</script>

	<!-- //here ends scrolling icon -->
<!-- scrolling script -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script> 
<!-- //scrolling script -->
<!-- animation effects-js files-->
	<script src="web/js/aos.js"></script><!-- //animation effects-js-->
	<script src="web/js/aos1.js"></script><!-- //animation effects-js-->
<!-- animation effects-js files-->

</body>	
</html>