-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 27, 2021 at 12:12 PM
-- Server version: 10.4.14-MariaDB-cll-lve
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u764488932_dms`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company`
--

CREATE TABLE `tbl_company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(100) NOT NULL DEFAULT '',
  `company_email` varchar(50) NOT NULL DEFAULT '',
  `company_no` varchar(20) NOT NULL DEFAULT '',
  `company_address` text NOT NULL,
  `company_image` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_company`
--

INSERT INTO `tbl_company` (`company_id`, `company_name`, `company_email`, `company_no`, `company_address`, `company_image`) VALUES
(1, 'Davao Branch', 'agmech.davao@gmail.com', '(6382) 22 76931', 'Prinsepe Bldg. Brgy. JP P. Laurel Ave, Bajada Davao City', 'logo.jpg'),
(2, 'Cebu Main Branch ', 'agmech.cebu@gmail.com', '0987654321', 'Cebu City', 'logo.jpg'),
(3, 'Cebu Branch ', 'agmech.mandaue@gmail.com', '(6332) 239-5401', 'Unit G-07,North Road Plaza Bldg, Labogon Road Mandaue City', 'logo.jpg'),
(4, 'Makati Branch', 'info@agmechsystem.com', '(632) 890-7205', 'G/F, CLF 1 BLDG., 1167 Chino Roses Ave, Makati City Philippines', 'logo.jpg'),
(5, 'Bacolod Main Branch', 'Bacolod_agmechsystems@gmail.com', '(6382) 22 76931', 'Purok Sabis, Brgy. Villamonte Bacolod City Negros Occ.', 'logo.jpg'),
(6, 'Talisay Branch', 'talisay_agmech@gmail.com', '031-5674-77865', 'Talisay City', 'agmech.jpg'),
(7, 'Davao Main Branch', 'davao@gmail.com', '039 702 8065', 'Davao City', 'agmechlogo.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dealer`
--

CREATE TABLE `tbl_dealer` (
  `dealer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dealer`
--

INSERT INTO `tbl_dealer` (`dealer_id`, `user_id`, `company_id`) VALUES
(5, 29, 1),
(7, 36, 5),
(8, 41, 6),
(9, 42, 0),
(10, 44, 7);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE `tbl_messages` (
  `message_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message_content` text NOT NULL,
  `message_datetime` datetime NOT NULL,
  `message_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_messages`
--

INSERT INTO `tbl_messages` (`message_id`, `sender_id`, `receiver_id`, `message_content`, `message_datetime`, `message_status`) VALUES
(2, 21, 20, 'test test test', '2020-09-23 10:00:00', 0),
(3, 21, 20, 'test test test sdasdasdas', '2020-09-23 10:00:00', 0),
(4, 20, 21, 'test test test sdasdasdas asdas das das  asd', '2020-09-23 10:00:00', 1),
(5, 21, 20, 'adsasdasdasdas', '2020-09-23 12:41:30', 0),
(6, 21, 20, 'test 2', '2020-09-23 12:41:46', 0),
(7, 21, 20, ' ds adas dasd asd', '2020-09-23 12:41:57', 0),
(8, 21, 20, 'dasdas das dsa das das d reert ert erqweqw', '2020-09-23 12:47:19', 0),
(9, 20, 21, 'rwasda sd asd asdasd as', '2020-09-23 12:51:05', 1),
(10, 1, 14, 'czxczx', '2020-09-23 14:01:20', 0),
(11, 1, 21, 'hi', '2020-09-23 14:03:29', 1),
(12, 1, 21, 'dsadas', '2020-09-23 14:21:31', 1),
(13, 21, 1, 'what', '2020-09-23 14:29:44', 0),
(14, 1, 29, 'hi', '2020-09-27 12:14:25', 0),
(15, 33, 29, 'Heyy', '2020-12-03 19:42:08', 0),
(16, 36, 33, 'Hi', '2020-12-15 23:52:01', 1),
(17, 39, 36, 'HI, Is this available?', '2021-01-04 18:48:12', 1),
(18, 36, 39, 'Yes.', '2021-01-04 18:49:18', 1),
(19, 39, 36, 'I already send the receipt.', '2021-01-04 19:04:54', 1),
(20, 39, 36, 'Can i send the receipt again?', '2021-01-06 19:11:18', 0),
(21, 40, 44, 'Hi', '2021-01-26 13:37:57', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(225) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `company_id` int(11) NOT NULL,
  `filename` varchar(100) NOT NULL DEFAULT '',
  `video` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `product_name`, `description`, `price`, `company_id`, `filename`, `video`, `date_added`) VALUES
(24, 'John Deere Flail Chopper', 'The rugged, simple design of the 972 Flail Chopper provides dependable performance. Two rows of double-edged knives cut the standing crop and lift it to a transfer auger.\r\nThe 12-in., 300-rpm auger then moves the material to a knife-equipped fan where three blades size the forage even further.\r\n', '2786354.04', 0, 'flail-chopper.jpg', 'https://www.youtube.com/watch?v=-wobjcHDdrM', '2020-10-22 02:23:32'),
(25, 'John Deere 5055 E 4WD', '5055 E 4WD Tractor has the comfort and convenience features to keep you smiling even during the longest days; the engine power and hydraulic capacity to take on hard-to-handle chores; and the quality of engineering, assembly, and components you expect from John Deere.', '4100565.03', 0, 'john-deere-5055-e-4wd-tractor-400.jpg', 'https://www.youtube.com/watch?v=008hwCVyLzs&t=18s', '2020-10-22 02:29:35'),
(26, 'John Deere 5045 D 4WD', '5045 D 4WD Tractor has the comfort and convenience features to keep you smiling even during the longest days; the engine power and hydraulic capacity to take on hard-to-handle chores; and the quality of engineering, assembly, and components you expect from John Deere.', '2340000.01', 0, 'abomar-tractor-5045-d-400px.jpg', 'https://www.youtube.com/watch?v=v4xFJM88w0s', '2020-10-22 02:33:11'),
(27, 'Tenias Evolution Front Loader', 'The new \'Evolution Series\' could only come from a top brand. This product is a further illustration of the decades-long on-going development in R&D&I here. Equipped with a DRP System for faster, safer coupling and uncoupling. The defining feature of this series is its structure of long-lasting robust arms.\r\nIts adaptability to all tractor types, flexibility and robustness comprise a high-performance loader.\r\n', '5250675.03', 0, 'tenias-front-loader-cebu-philippines.jpg', 'https://www.youtube.com/watch?v=16Q1_AN_EDw', '2020-10-22 02:35:55'),
(28, 'John Deere 3955 Forage Harvester', 'Get better quality-of-cut while using less fuel. The 3955 Pull-Type Forage Harvesters deliver more tons of crop per hour while using less fuel.', '12100650.00', 0, 'forage-harvester.jpg', 'https://www.youtube.com/watch?v=eLD2oDxLkgQ', '2020-10-22 02:38:36'),
(29, 'John Deere HX6 Rotary Cutter', 'The HX6 Rotary Cutter is a double-deck design with stump jumpers and suction blades, just like the other John Deere heavy-duty rotary cutters. The HX6 is available in a 540-rpm tractor power take-off (PTO) lift-type configuration. Each HX6 Rotary Cutter comes with bolt-on replaceable skid shoes, saving time and money when they need to be replaced.', '5650000.01', 0, 'hx6-rotary-cutter.jpg', 'https://www.youtube.com/watch?v=-1mhdMf5gkw&t=99s', '2020-10-22 02:41:07'),
(30, 'John Deere 3036E', 'John Deere 3036E tractor is specifically designed and developed for the Asian market. This tractor is a product of a 5-year study, research and thousands of hours testing in different locations of Thailand, trying to understand the local customer needs for agricultural machinery.', '2355065.00', 0, 'john-deere-tractor-3036E.jpg', 'https://www.youtube.com/watch?v=nIrfSNv4EZo', '2020-10-22 05:17:23'),
(31, 'John Deere H240 Loader', 'The H240 Loader offers 5E Series Tractor operators a durable and reliable loader for moderate-duty work. It is available in self-leveling and non-self leveling configurations and includes a loader suspension system for improved productivity. Quik-Park™ stands make it easy to attach and remove without tools. And concealed oil lines improve visibility while reducing the potential for damage.', '5256000.00', 0, 'john-deere-h240-tractor-loader.jpg', 'https://www.youtube.com/watch?v=WDwBHn63xg8', '2020-10-22 05:23:00'),
(32, 'John Deere 512 Loader', 'The 512 Loader is designed for operators who do not require the loader to be removed for use of the tractor for other applications where the loader is not required. The normal mast and mounting frame components have been combined into one component, the mounting frame, which is solid-mounted to the tractor chassis.', '3654000.01', 0, '7624402_huge.jpg', 'https://www.youtube.com/watch?v=ZWjvM65buuY', '2020-10-22 05:35:40'),
(33, 'Corn Harvester', 'less fuel', '2000000.00', 0, '3975-forage-harvester.jpg', 'nIrfSNv4EZo', '2020-10-29 16:31:10'),
(34, '6525 120 HP tractor', 'The fullframe design is more rigid and distributes implement weight evenly across the entire frame. Less stress is placed on the drivetrain and components. And it allows for perfect integration of John Deere loaders. In fact, this design is used in all 6000 series tractors for unmatched reliability and strength.', '5000000.00', 0, 'blogger-image-233271754.jpg', '', '2021-01-20 15:36:21'),
(35, 'John Deere 5115R Tractor', '20 5115R 450h ? PowerGard Certified | Command8 32F16R PwrRev LED Prem Cab Suspension 540/1000 3 Function E Joystick 3 EScv Cmd Arm | New From $/y 2.99% | Includes Premium John Deere Service & Inspection | Replacement of All Machine Filters | JD Plus 50 II Engine Oil Change', '12000000.00', 0, '4wd-tractors-5115-r-john-deere.jpg', 'https://www.youtube.com/watch?v=B_1dQnbOzBQ', '2021-01-26 05:19:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_review`
--

CREATE TABLE `tbl_review` (
  `review_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ref_num` varchar(225) NOT NULL,
  `product_id` int(11) NOT NULL,
  `remarks` text NOT NULL,
  `date_added` datetime NOT NULL,
  `star_value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_review`
--

INSERT INTO `tbl_review` (`review_id`, `user_id`, `ref_num`, `product_id`, `remarks`, `date_added`, `star_value`) VALUES
(1, 1, 'RN-1060820091427', 7, 'dsadas', '2020-06-08 11:11:30', 5),
(2, 1, 'RN-1060820091427', 7, 'dasdsada', '2020-06-08 11:11:30', 4),
(3, 1, 'RN-1060820091427', 4, 'asdsa', '2020-07-11 11:16:30', 3),
(4, 39, 'RN-39010421060609', 27, 'Thank you seller.', '2021-01-04 20:38:27', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supply`
--

CREATE TABLE `tbl_supply` (
  `supply_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_supply`
--

INSERT INTO `tbl_supply` (`supply_id`, `product_id`, `company`, `qty`, `date_added`, `user_id`) VALUES
(18, 27, 5, 10, '2020-10-22', 2),
(19, 31, 0, 10, '2020-10-22', 2),
(20, 30, 4, 10, '2020-10-22', 2),
(22, 26, 4, 10, '2020-10-22', 2),
(23, 34, 0, 10, '2020-10-20', 2),
(24, 35, 7, 10, '2021-01-26', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_track_transaction`
--

CREATE TABLE `tbl_track_transaction` (
  `track_id` int(11) NOT NULL,
  `ref_num` text NOT NULL,
  `module` text NOT NULL,
  `date_added` datetime NOT NULL,
  `read_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_track_transaction`
--

INSERT INTO `tbl_track_transaction` (`track_id`, `ref_num`, `module`, `date_added`, `read_status`) VALUES
(3, 'RN-1060620110134', 'PC', '2020-05-27 13:45:37', 0),
(10, 'RN-1060620110134', 'DL', '2020-05-27 14:33:33', 0),
(11, '4', 'PL', '2020-05-27 14:33:38', 0),
(14, '4', 'DC', '2020-05-27 14:36:14', 0),
(15, 'RN-1071120024038', 'Order has been  successfully confirmed by the dealer', '2020-07-15 14:52:18', 0),
(16, 'RN-1090520103813', 'Order has been  successfully confirmed by the dealer', '2020-09-05 10:40:03', 1),
(18, 'RN-1090520103813', 'Your order is ready to ship from the seller', '2020-09-05 10:56:28', 1),
(19, 'RN-1090520103813', 'Order has successfully been deliver', '2020-09-05 15:05:38', 1),
(20, '', 'Order has been  successfully confirmed by the dealer', '2020-09-25 10:05:32', 1),
(21, 'RN-39010421060609', 'Order has been  successfully confirmed by the dealer', '2021-01-04 18:48:54', 1),
(22, 'RN-39010421060609', 'Your order is ready to ship from the seller', '2021-01-04 20:36:22', 1),
(23, 'RN-39010421060609', 'Order has successfully been deliver', '2021-01-04 20:37:37', 1),
(24, 'RN-40011121013457', 'Order has been  successfully confirmed by the dealer', '2021-01-11 15:02:01', 1),
(25, 'RN-40011121013754', 'Order has been  successfully confirmed by the dealer', '2021-01-11 15:02:19', 0),
(26, 'RN-40011121030107', 'Order has been  successfully confirmed by the dealer', '2021-01-11 15:08:47', 0),
(27, 'RN-40011121013754', 'Your order is ready to ship from the seller', '2021-01-11 16:05:22', 0),
(28, 'RN-40011121013457', 'Your order is ready to ship from the seller', '2021-01-11 16:05:27', 0),
(29, 'RN-40011121013754', 'Order has successfully been deliver', '2021-01-11 16:06:58', 0),
(30, 'RN-40011121013457', 'Order has successfully been deliver', '2021-01-11 16:07:03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction`
--

CREATE TABLE `tbl_transaction` (
  `trans_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `dealer_id` int(11) NOT NULL,
  `status` varchar(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_finish` datetime NOT NULL,
  `price` decimal(12,3) NOT NULL,
  `qty` int(11) NOT NULL,
  `ref_num` text NOT NULL,
  `ship_opt` int(1) NOT NULL,
  `receipt_img` varchar(100) NOT NULL,
  `cart_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaction`
--

INSERT INTO `tbl_transaction` (`trans_id`, `user_id`, `company_id`, `product_id`, `dealer_id`, `status`, `date_added`, `date_finish`, `price`, `qty`, `ref_num`, `ship_opt`, `receipt_img`, `cart_status`) VALUES
(1, 1, 2, 7, 14, 'F', '2020-06-08 09:14:27', '2020-07-10 11:09:34', '1200.000', 2, 'RN-1060820091427', 0, '', 0),
(8, 1, 2, 4, 14, 'F', '2020-06-05 11:56:07', '2020-07-15 11:09:34', '123.000', 10, 'RN-1060820091427', 0, '', 0),
(13, 1, 1, 6, 21, 'F', '2020-09-05 10:38:13', '2020-09-05 12:05:38', '65230.000', 1, 'RN-1090520103813', 1, '', 0),
(14, 1, 1, 7, 21, 'C', '2020-07-11 14:40:38', '0000-00-00 00:00:00', '450.000', 2, '', 0, '', 0),
(15, 1, 1, 4, 14, 'X', '2020-09-19 20:34:51', '0000-00-00 00:00:00', '10000.000', 1, 'RN-1091920083451', 0, '', 0),
(16, 30, 1, 6, 0, 'P', '2020-10-22 08:28:54', '0000-00-00 00:00:00', '650.000', 3, '', 0, '', 0),
(17, 33, 0, 30, 0, 'X', '2020-12-15 23:58:29', '0000-00-00 00:00:00', '2355065.000', 1, 'RN-33121520115829', 0, '', 0),
(18, 33, 0, 27, 36, 'P', '2020-12-18 18:21:41', '0000-00-00 00:00:00', '5250675.030', 28, '', 0, '', 0),
(19, 1, 0, 27, 36, 'P', '2021-01-12 11:10:35', '0000-00-00 00:00:00', '5250675.030', 1, 'RN-1010421054053', 0, '', 0),
(20, 38, 0, 27, 36, 'P', '2021-01-04 17:44:47', '0000-00-00 00:00:00', '5250675.030', 10, '', 0, '', 0),
(21, 39, 6, 27, 36, 'F', '2021-01-04 18:06:09', '2021-01-04 20:37:37', '5250675.030', 2, 'RN-39010421060609', 0, 'receipts-1200.jpg', 0),
(22, 39, 0, 27, 36, 'P', '2021-01-10 22:27:06', '0000-00-00 00:00:00', '5250675.030', 11, '', 0, '', 0),
(23, 40, 6, 27, 36, 'F', '2021-01-11 13:34:57', '2021-01-11 16:07:03', '5250675.030', 1, 'RN-40011121013457', 0, 'receipts-1200.jpg', 0),
(24, 40, 6, 27, 36, 'F', '2021-01-11 13:37:54', '2021-01-11 16:06:58', '5250675.030', 1, 'RN-40011121013754', 0, 'receipts-1200.jpg', 0),
(25, 40, 6, 27, 36, 'F', '2021-01-11 15:01:07', '0000-00-00 00:00:00', '5250675.030', 1, 'RN-40011121030107', 0, 'receipts-1200.jpg', 0),
(26, 40, 0, 27, 36, 'P', '2021-01-19 23:34:12', '0000-00-00 00:00:00', '5250675.030', 3, 'RN-40011921113412', 0, '', 0),
(27, 40, 0, 35, 44, 'S', '2021-01-26 13:36:56', '0000-00-00 00:00:00', '12000000.000', 1, 'RN-40012621013656', 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `fname` varchar(225) NOT NULL,
  `lname` varchar(225) NOT NULL,
  `bday` date NOT NULL,
  `contact_number` varchar(225) NOT NULL,
  `address` text NOT NULL,
  `un` varchar(225) NOT NULL,
  `pw` varchar(225) NOT NULL,
  `status` varchar(5) NOT NULL DEFAULT '',
  `date_added` datetime NOT NULL,
  `email` varchar(255) NOT NULL,
  `filename` text NOT NULL,
  `ishidden` int(1) NOT NULL,
  `suffix` varchar(5) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `company_id`, `fname`, `lname`, `bday`, `contact_number`, `address`, `un`, `pw`, `status`, `date_added`, `email`, `filename`, `ishidden`, `suffix`, `created_by`) VALUES
(1, 0, 'John', 'Sy', '2020-05-22', '091075059596', 'Tangub', 'q', 'q', 'C', '2020-05-22 00:00:00', 's@gmail.com', '1591595065.png', 0, '', 0),
(2, 0, 'Product', 'Manager', '1989-05-22', '+1 309- 765- 8000', 'One John Deere PI, Moline, IL 61265, United States', 'agmech', 'agmech', 'A', '2020-05-22 00:00:00', 'Johndeerecorporatesystem@mail.com', '1603331677.png', 1, '', 0),
(3, 0, 'asdas', 'dsad', '2020-05-27', '1412', '', 'das', 'dsa', 'C', '2020-05-22 00:00:00', '', '', 0, '', 0),
(4, 0, 'sadsa', 'das', '2020-05-01', '3423', '', 'sdas', 'dsa', 'C', '2020-05-22 00:00:00', '', '', 0, '', 0),
(5, 0, 'das', 'dasda', '2020-05-22', '414', '', '1241', 'q', 'C', '2020-05-22 18:58:27', '', '', 0, '', 0),
(17, 0, 'John', 'Doe', '2020-05-26', '42343', 'rewr', 'jd', '12345', 'L', '2020-05-26 01:11:20', 'dasd@GMAIL.com', '', 0, '', 0),
(18, 0, 'q', 'qqq', '0000-00-00', '21321312 ', 'dasd ', 'l', '12345', 'L', '2020-05-26 01:12:34', '41234 ', '1590643326.png', 0, '', 0),
(20, 1, 'Juvia', 'Lockser', '1996-04-22', '09876543211', 'Brgy. Tangub Bacolod City', 'juvia', '123456', 'CO', '2020-06-09 16:16:08', 'juvia@gmail.com', 'avatar.png', 0, '', 0),
(24, 3, 'fname', 'lname', '2000-01-01', '09123456789', 'Rizal St, Bacolod, 6100 Negros Occidental', 'testcom', '12345', 'CO', '2020-09-05 10:02:23', 'fshj@gmail.com', '1603182338.png', 0, '', 0),
(25, 4, 'dsadsa', 'dsad', '2000-01-01', 'dsad', 'sadas', 'makati', 'makati', 'CO', '2020-09-05 10:05:34', 'email@gmail.com', 'Crowd-Size-Estimation-Using-OpenCV-and-Raspberry-Pi.jpg', 0, '', 0),
(26, 0, '', '', '0000-00-00', '', '', 'zzz', 'z', 'C', '2020-09-19 10:22:00', '', '', 0, '', 0),
(27, 0, '', '', '0000-00-00', '', '', 'c', 'c', 'C', '2020-09-19 10:27:43', '', '', 0, '', 0),
(28, 0, '', '', '0000-00-00', '', '', 't', 't', 'C', '2020-09-19 10:37:20', '', '', 0, '', 0),
(29, 1, 'Xian Reign', 'Canlas', '0000-00-00', '09107505919', 'Bacolod City', 'Xian Reign', '12345', 'D', '2020-09-19 11:53:16', 'xain@gmail.com', '', 0, '', 0),
(30, 0, '', '', '0000-00-00', '', '', 'joy', 'joy', 'C', '2020-10-22 08:27:58', '', '', 0, '', 0),
(31, 0, '', '', '0000-00-00', '', '', 'Khiezzy', '123', 'C', '2020-10-22 17:24:51', '', '', 0, '', 0),
(32, 0, '', '', '0000-00-00', '', '', 'kz', 'kz', 'C', '2020-10-29 17:56:02', '', '', 0, '', 0),
(33, 0, 'Mariel', 'Lao', '1991-02-23', '09500896789', 'Bacolod City', 'Mikee', 'mikee', 'C', '2020-12-03 19:41:17', 'mareillao@gmail.com', '1607426650.png', 0, '', 0),
(35, 6, 'Felix', 'Hugh', '1975-06-16', '103-7658-0923', 'Brgy. Alijis, Bacolod City', 'felix', '123', 'CO', '2020-12-10 08:23:06', 'felixhugh@gmail.com', '1607588714.png', 0, '', 0),
(36, 6, 'Daniel', 'Vogue', '1975-12-20', '09100256980', 'Cebu City', 'Jack', '12345', 'D', '2020-12-10 08:29:57', 'danielvogue@gmail.com', '1608047454.png', 0, '', 0),
(37, 5, 'Daniel', 'Vogue', '1975-12-20', '09100256980', 'Cebu City', 'Dealer1', '12345', 'D', '2020-12-10 08:29:57', 'danielvogue@gmail.com', '1608047454.png', 0, '', 0),
(38, 0, 'Mariel', 'Tan', '1995-10-23', '09500896789', 'Alijis', 'mariel tan', 'tan', 'C', '2021-01-04 17:43:48', 'mareillao@gmail.com', '1609753577.png', 0, '', 0),
(39, 0, 'Mariel', 'Sue', '1995-01-04', '09500896765', 'Cadiz City', 'Mariel Lee', 'lee', 'C', '2021-01-04 17:53:36', 'mareilsue@gmail.com', '1609754394.png', 0, '', 0),
(40, 0, 'Mariel', 'Sy', '1986-12-23', '09124567345', 'Victorias City', 'Mariel', 'lee', 'C', '2021-01-11 13:25:31', 'marielsy@gmail.com', '1610343036.png', 0, '', 0),
(41, 6, 'Carlo', 'Woo', '0000-00-00', '09234567881', 'Bacolod City', 'Carlo', '12345', 'D', '2021-01-20 15:43:12', 'carlowoo@gmail.com', '', 0, '', 0),
(42, 0, 'karren', 'Davilla', '0000-00-00', '09123423422', 'Cebu City', 'karren', '12345', 'D', '2021-01-26 04:27:57', 'karren', '', 0, '', 0),
(43, 7, 'Karren', 'Davillla', '1980-02-16', '090987654678', 'Cebu City', 'Karren', '123', 'CO', '2021-01-26 05:10:08', 'karrendavilla@gmail.com', '1611638053.png', 0, '', 0),
(44, 7, 'George', 'Johnsom', '0000-00-00', '09997837156', 'Bacolod City', 'George', '12345', 'D', '2021-01-26 05:15:53', 'georgejohnson@gmail.com', '', 0, '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_company`
--
ALTER TABLE `tbl_company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `tbl_dealer`
--
ALTER TABLE `tbl_dealer`
  ADD PRIMARY KEY (`dealer_id`);

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_review`
--
ALTER TABLE `tbl_review`
  ADD PRIMARY KEY (`review_id`);

--
-- Indexes for table `tbl_supply`
--
ALTER TABLE `tbl_supply`
  ADD PRIMARY KEY (`supply_id`);

--
-- Indexes for table `tbl_track_transaction`
--
ALTER TABLE `tbl_track_transaction`
  ADD PRIMARY KEY (`track_id`);

--
-- Indexes for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  ADD PRIMARY KEY (`trans_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_company`
--
ALTER TABLE `tbl_company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_dealer`
--
ALTER TABLE `tbl_dealer`
  MODIFY `dealer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tbl_review`
--
ALTER TABLE `tbl_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_supply`
--
ALTER TABLE `tbl_supply`
  MODIFY `supply_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tbl_track_transaction`
--
ALTER TABLE `tbl_track_transaction`
  MODIFY `track_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_transaction`
--
ALTER TABLE `tbl_transaction`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
