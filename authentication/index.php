<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>DMS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- App favicon -->
        <link rel="icon" href="../web/images/logo.png" type="image/icon type">
        <!-- App css -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/metisMenu.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="../assets/plugins/animate/animate.css" rel="stylesheet" type="text/css" />

    </head>

    <body class="account-body accountbg" >
        <!-- Log In page -->
        <div class="row vh-100 ">
            <div class="col-12 align-self-center" >
                <div class="auth-page" style="margin-left: 58% !important;    top: 25px;">
                    <div class="card auth-card shadow-lg">
                        <div class="card-body">
                            <div class="px-3">
                                <div class="auth-logo-box">
                                    <a href="#" class="logo logo-admin"><img src="../web/images/logo.png" height="55" alt="logo" class="auth-logo"></a>
                                </div><!--end auth-logo-box-->
                                
                                <div class="text-center auth-logo-text">
                                    <h4 class="mt-0 mb-3 mt-5">Let's Get Started </h4>
                                    <p class="text-muted mb-0">Sign in to continue .</p>  
                                </div> <!--end auth-logo-text-->  

                                <form class="form-horizontal auth-form my-4" id="loginForm">
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <div class="input-group mb-3">
                                            <span class="auth-form-icon">
                                                <i class="dripicons-user"></i> 
                                            </span>                                                                                                              
                                            <input type="text" class="form-control" id="username" name="username" placeholder="Enter username" autocomplete='off' autofocus>
                                        </div>                                    
                                    </div><!--end form-group--> 
        
                                    <div class="form-group">
                                        <label for="userpassword">Password</label>                                            
                                        <div class="input-group mb-3"> 
                                            <span class="auth-form-icon">
                                                <i class="dripicons-lock"></i> 
                                            </span>                                                       
                                            <input type="password" class="form-control" id="userpassword" name="password" placeholder="Enter password">
                                        </div>                               
                                    </div><!--end form-group--> 
        
                                    <div class="form-group mb-0 row">
                                        <div class="col-12 mt-2">
                                            <button class="btn btn-primary btn-round btn-block waves-effect waves-light" type="submit" style="background-color: #033f03;border: 1px solid #013a01;">Log In <i class="fas fa-sign-in-alt ml-1"></i></button>
                                        </div><!--end col--> 
                                    </div> <!--end form-group-->                           
                                </form><!--end form-->
                            </div><!--end /div-->


                            <div class="m-3 text-center text-muted">
                               <p class="animated" id="notif_p"> <span id="notif" > </span></p>
                            </div>

                           <!--  <div class="m-3 text-center text-muted">
                                <p class="">Don't have an account ?  <a href="../authentication/auth-register.php" class="text-primary ml-2">Free Register</a></p>
                            </div> -->
                        </div><!--end card-body-->
                    </div><!--end card-->
                 
                </div><!--end auth-page-->
            </div><!--end col-->           
        </div><!--end row-->
        <!-- End Log In page -->
    

        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.bundle.min.js"></script>
        <script src="../assets/js/metisMenu.min.js"></script>
        <script src="../assets/js/waves.min.js"></script>
        <script src="../assets/js/jquery.slimscroll.min.js"></script>

        <!-- App js -->
        <script src="../assets/js/app.js"></script>

    </body>
</html>
<script type="text/javascript">
  $(document).ready(function(){

    $("#loginForm").submit(function(e){
    e.preventDefault();
    $("#notif_p").removeClass("shake");
    $("#notif_p").removeClass("fadeIn");

     $.ajax({
        url:"../ajax/login_user.php",
        method:"POST",
        data:$(this).serialize(),
        success: function(data){
          if(data!=0){
            $("#notif_p").toggleClass("fadeIn");
            $("#notif").html("<span class=' alert alert-success'>Successfully Login</span>");
            setTimeout(function(){
                 window.location.replace("../ecommerce/index.php?page="+data);
            },1000);
         }else{
            $("#notif_p").toggleClass("shake");
             $("#notif").html("<span class='alert alert-danger'>Incorrect Password or Username</span>");
          }
        }
      });
    });

  })
</script>