<?php 
 include "../core/config.php";
 $id=$_GET['id'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Lockscreen</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- App favicon -->
        <link rel="icon" href="../web/images/logo.png" type="image/icon type">

        <!-- App css -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/metisMenu.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />

    </head>

    <body class="account-body accountbg">

            <!-- Log In page -->
            <div class="row vh-100 ">
                <div class="col-12 align-self-center">
                    <div class="auth-page" style="margin-left: 58% !important;    top: 25px;">
                        <div class="card auth-card shadow-lg">
                            <div class="card-body">
                                <div class="px-3">
                                    <div class="auth-logo-box">
                                        <a href="#" class="logo logo-admin"><img src="../web/images/logo.png" height="55" alt="logo" class="auth-logo"></a>
                                    </div><!--end auth-logo-box-->
                                    
                                    <div class="text-center auth-logo-text">
                                        <h4 class="mt-0 mb-3 mt-5">Enter Password</h4>
                                        <p class="text-muted mb-0">Hello <?php echo getFullname($id);?>, enter your password to unlock the screen !</p>  
                                    </div> <!--end auth-logo-text-->  

                                    
                                    <form class="form-horizontal auth-form my-4" id="lockscreen">
                                        <div class="user-thumb text-center m-b-30">
                                            <img src="../assets/images/user_image/<?php echo ucwords(getData($id,'tbl_user','filename','user_id'));?>" class="rounded-circle img-thumbnail thumb-xl" alt="thumbnail">
                                            <h5><?php echo getFullname($id);?></h5>
                                        </div>  
                                         <input type="hidden" class="form-control" name="user_id"  value="<?php echo $id;?>">   
                                        <div class="form-group">
                                            <label for="userpassword">Password</label>                   
                                                                  
                                            <div class="input-group mb-3"> 
                                                <span class="auth-form-icon">
                                                    <i class="dripicons-lock"></i> 
                                                </span>                                                       
                                                <input type="password" class="form-control" name="password" id="userpassword" placeholder="Enter password">
                                            </div>                               
                                        </div><!--end form-group--> 
                                        
            
                                        <div class="form-group mb-0 row">
                                            <div class="col-12 mt-2">
                                                <button type="submit" class="btn btn-primary btn-round btn-block waves-effect waves-light" type="submit">Unlock <i class="fas fa-sign-in-alt ml-1"></i></button>
                                            </div><!--end col--> 
                                        </div> <!--end form-group-->                           
                                    </form><!--end form-->
                                </div><!--end /div-->
                                <div class="m-3 text-center text-muted">
                               <p class="animated" id="notif_p"> <span id="notif" > </span></p>
                            </div>
                                
                                <div class="m-3 text-center text-muted">
                                    <p class="">Not you ? return  <a href="../authentication/" class="text-primary ml-2">Sign In</a></p>
                                </div>
                            </div><!--end card-body-->
                        </div><!--end card-->
                    </div><!--end auth-page-->
                </div><!--end col-->           
            </div><!--end row-->
            <!-- End Log In page -->

        <!-- jQuery  -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.bundle.min.js"></script>
        <script src="../assets/js/metisMenu.min.js"></script>
        <script src="../assets/js/waves.min.js"></script>
        <script src="../assets/js/jquery.slimscroll.min.js"></script>

        <!-- App js -->
        <script src="../assets/js/app.js"></script>

    </body>
</html>
<script type="text/javascript">
  $(document).ready(function(){

    $("#lockscreen").submit(function(e){
    e.preventDefault();
    $("#notif_p").removeClass("shake");
    $("#notif_p").removeClass("fadeIn");
     $.ajax({
        url:"../ajax/lockscreen_check.php",
        method:"POST",
        data:$(this).serialize(),
        success: function(data){
          if(data == 1){
            $("#notif_p").toggleClass("fadeIn");
            $("#notif").html("<span class=' alert alert-success'>Successfully Login</span>");
            setTimeout(function(){
                 window.location.replace("../ecommerce/index.php?page=products");
            },1000);
         }else{
            $("#notif_p").toggleClass("shake");
             $("#notif").html("<span class='alert alert-danger'>Incorrect Password</span>");
          }
        }
      });
    });

  })
</script>