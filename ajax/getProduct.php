<div class="container-fluid">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="float-right">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0);">DMS</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Ecommerce</a></li>
                        <li class="breadcrumb-item active">Search Products</li>
                    </ol>
                </div>
                <h4 class="page-title">Search Products</h4>
            </div><!--end page-title-box-->
        </div><!--end col-->
    </div>
    <!-- end page title end breadcrumb -->
 <div class="row">  <?php
include '../core/config.php';
// Company Details
$search = $_POST['search'];
        $fetch = mysql_query("SELECT * from tbl_product where product_name like '%$search%'");

        $count_prod = mysql_num_rows($fetch);

        if($count_prod == 0){?>
            <div class="col-lg-12" >
                <div class="card e-co-product">                                  
                    <div class="card-body product-info">
                        <a href="" class="product-title">Product Not found!</a>
                    </div><!--end card-body-->
                </div><!--end card-->
            </div><!--end col-->

        <?php }else{
            while($row = mysql_fetch_array($fetch)){
            $id= $row['product_id'];
            $count = mysql_fetch_array(mysql_query("SELECT avg(star_value) FROM tbl_review where product_id='$id'"));
            $rating = number_format($count[0],2);
             $sd = date('Y-m-d');

            $count_supply = mysql_fetch_array(mysql_query("SELECT sum(qty) FROM tbl_supply where product_id='$id' and DATE_FORMAT(date_added, '%Y-%m-%d') <=  '$sd' "));
            $count_trans = mysql_fetch_array(mysql_query("SELECT sum(qty) FROM tbl_transaction where product_id='$id' and status='F' and DATE_FORMAT(date_finish, '%Y-%m-%d') <=  '$sd' "))or die (mysql_error());


             $diff = $count_supply[0] - $count_trans[0];

            if($diff<=0){
                $onclick = "";
                $addToCart = '<button class="btn btn-danger btn-sm waves-effect waves-light" disabled> Sold out </button>';
            }else{
                $onclick = "onclick='productdetails(".$row['product_id'].")'";
                $addToCart = '<button class="btn btn-success  btn-sm waves-effect waves-light"><i class="mdi mdi-cart mr-1"></i> Add To Cart</button>';

            }

            ?>                   
            <div class="col-lg-3" <?= $onclick;?> >
                <div class="card e-co-product">
                    <a href="#">  
                        <img src="../assets/images/products/<?php echo $row['filename'];?>" alt="" class="img-fluid">
                    </a>                                    
                    <div class="card-body product-info">
                        <a href="" class="product-title"><?php echo substr($row['product_name'],0,24);?></a>
                        <div class="d-flex justify-content-between my-2">
                            <p class="product-price">&#8369; <?php echo $row['price'];?></p>

                            <ul class="list-inline mb-0 product-review align-self-center">
                                <?php 
                                $start=1;
                                while($start<=5){
                                    if($start<=$rating){
                                        $style='text-warning';
                                        $diff = 0 ;
                                        $half = "";
                                    }else{
                                        $diff = abs($rating - $start);
                                        $style='text-none';
                                        if($diff<1 && $diff>0){
                                            $style='text-warning';
                                            $half = "-half";
                                        }else{
                                            $half = "";
                                            $style='text-none';
                                        }
                                    }
                                ?>
                                <li class="list-inline-item"><i class="mdi mdi-star<?php echo $half;?> <?php echo $style;?>"></i></li>
                                <?php $start++;}
                                ?>
                            </ul>
                        </div>
                        <?= $addToCart;?> 
                    </div><!--end card-body-->
                </div><!--end card-->
            </div><!--end col-->
            <?php } }?>
       
    </div>
</div>
<script type="text/javascript">
  function productdetails(id){
        window.location.replace("index.php?page=productDetail&id="+id);
  }
</script>