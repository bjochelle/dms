<?php 
  include "../core/config.php";
  $sd = $_POST['sd'];
  $ed = $_POST['ed'];
  $company_id= $_POST['company_id'];

  session_start();
  $status = $_SESSION['status'];
  $id= $_SESSION['id'];

    


  if($status == 'A'){
    if($company_id == '-1'){
        $add_query = "";
        $info = "All Branch";
    }else{
        $add_query = "and company_id=".$company_id;
        $info = "Branch :".getData($company_id,'tbl_company','company_name','company_id');
    }
  }else if($status == 'D'){
        $add_query = "and dealer_id=".$id;
        $info = "Branch :".getData($company_id,'tbl_company','company_name','company_id');
  }else{
        $add_query = "and company_id=".$company_id;
        $info = "Branch :".getData($company_id,'tbl_company','company_name','company_id');
  }

?>
<div class="row invoice-head">
    <div class="col-md-4 align-self-center">                                                
        <img src="../web/images/logo.png" alt="logo-small" class="logo-sm mr-2" height="46"><strong style="font-size: 26px;">EDMS </strong>                                               
    </div>
    <div class="col-md-8 ">
            
        <ul class="list-inline mb-0 contact-detail float-right">                                                   
            <li class="list-inline-item">
                <div class="pl-3">
                    <i class="mdi mdi-web"></i>
                    <p class="text-muted mb-0">www.dms.chmsc.tech</p>
                </div>                                                
            </li>
            <li class="list-inline-item">
                <div class="pl-3">
                    <i class="mdi mdi-phone"></i>
                    <p class="text-muted mb-0">+639 123456789</p>
                </div>
            </li>
            <li class="list-inline-item">
                <div class="pl-3">
                    <i class="mdi mdi-map-marker"></i>
                    <p class="text-muted mb-0">Rizal St, Bacolod</p>
                    <p class="text-muted mb-0">6100 Negros Occidental.</p>
                </div>
            </li>
        </ul>
    </div>
</div> 
<div class="card-body">
<div class="row">
    <div class="col-md-6">
        <div class="">
            <h6 class="mb-0"><b>From Date :</b> <?php echo date("m/d/Y",strtotime($sd));?></h6>
        </div>
    </div>
    <div class="col-md-6">
        <div class="">
            <h6 class="mb-0"><b>To Date :</b> <?php echo date("m/d/Y",strtotime($ed));?></h6>
        </div>
    </div>                                      
    
    <div class="col-md-12">
        <div class="text-center p-3 mb-3">
            <h5>Sales Report</h5>
            <h6><?php echo $info;?></h6>

        </div>                                              
    </div>                                            
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered mb-0">
                <thead>
                    <tr>
                        <th>Quantity</th>
                        <th>Item</th>                                                   
                        <th>Unit Cost</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                  <?php 

                  $fetch = mysql_query("SELECT * FROM tbl_transaction where status='F' and DATE_FORMAT(date_finish, '%Y-%m-%d') >=  '$sd' and DATE_FORMAT(date_finish, '%Y-%m-%d') <=  '$ed' $add_query");
                  $final_total = 0;
                  while($row = mysql_fetch_array($fetch)){
                    $product_id = $row['product_id'];
                    $total = $row['price']*$row['qty'];
                    $final_total+=$total; ?>
                    <tr>
                        <th><?php echo $row['qty'];?></th>
                        <td><?php echo getData($product_id,'tbl_product','product_name','product_id')?></td>
                        <td>&#8369; <?php echo number_format($row['price'],2);?></td>
                        <td>&#8369; <?php echo number_format($total,2);?></td>
                    </tr>
                  <?php }?>
                  <tr class="bg-dark text-white">
                    <th colspan="2" class="border-0"></th>                                                        
                    <td class="border-0 font-14"><b>Total</b></td>
                    <td class="border-0 font-14"><b>&#8369; <?php echo number_format($final_total,2);?></b></td>
                </tr>
                </tbody>
            </table>
        </div>                                            
    </div>                                        
</div>
 <br><br><br>
<div class="row justify-content-center">       
    <div class="col-lg-12 align-self-end">
        <div class="w-25 float-right">
            <small>Account Manager</small>
            <br><br><br>
            
            <p class="border-top"><?php echo ucwords($_SESSION['name']);?><br>Signature</p>
        </div>
    </div>
</div>
<hr>
</div>