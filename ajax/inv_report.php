<?php 
  include "../core/config.php";
  $sd = $_POST['sd'];
  $company_id= $_POST['company_id'];

  session_start();
  $status = $_SESSION['status'];
  $id= $_SESSION['id'];

  // if($status == 'A'){
    if($company_id == '-1'){
        $add_query = "";
        $add_query1 = "";


        $info = "All Branch";
    }else{
        $add_query = "and company=".$company_id;
        $add_query1 = "and company_id=".$company_id;

        $info = "Branch :".getData($company_id,'tbl_company','company_name','company_id');
    }
  // }else if($status == 'D'){
  //       $add_query = "and company=".$id;
  //       $info = "Branch :".getData($company_id,'tbl_company','company_name',$company_id);
  // }else{
  //       $add_query = "and company=".$company_id;
  //       $info = "Branch :".getData($company_id,'tbl_company','company_name',$company_id);
  // }


?>
<div class="row invoice-head">
    <div class="col-md-4 align-self-center">                                                
        <img src="../web/images/logo.png" alt="logo-small" class="logo-sm mr-2" height="46"><strong style="font-size: 26px;">EDMS </strong>                                               
    </div>
    <div class="col-md-8 ">
            
        <ul class="list-inline mb-0 contact-detail float-right">                                                   
            <li class="list-inline-item">
                <div class="pl-3">
                    <i class="mdi mdi-web"></i>
                    <p class="text-muted mb-0">www.dms.chmsc.tech</p>
                </div>                                                
            </li>
            <li class="list-inline-item">
                <div class="pl-3">
                    <i class="mdi mdi-phone"></i>
                    <p class="text-muted mb-0">+639 123456789</p>
                </div>
            </li>
            <li class="list-inline-item">
                <div class="pl-3">
                    <i class="mdi mdi-map-marker"></i>
                    <p class="text-muted mb-0">Rizal St, Bacolod</p>
                    <p class="text-muted mb-0">6100 Negros Occidental.</p>
                </div>
            </li>
        </ul>
    </div>
</div> 
<div class="card-body">
<div class="row">
  
    <div class="col-md-12">
        <div class="text-center p-3 mb-3">
            <h5>Inventory Report</h5>
            <h6><?php echo $info;?></h6>
            <b>Inventory Date :</b> <?php echo date("m/d/Y",strtotime($sd));?></h6>

        </div>                                              
    </div>                                            
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered mb-0">
                <thead>
                    <tr class="bg-dark text-white" >
                        <th class="text-white">Item</th>
                        <th class="text-white">Quantity</th>   
                    </tr>
                </thead>
                <tbody>
                  <?php 

                  $fetch = mysql_query("SELECT * FROM tbl_product");
                  while($row = mysql_fetch_array($fetch)){
                    $product_id = $row['product_id'];
                    $count_supply = mysql_fetch_array(mysql_query("SELECT sum(qty) FROM tbl_supply where product_id='$product_id' and DATE_FORMAT(date_added, '%Y-%m-%d') <=  '$sd' $add_query"));
                    $count_trans = mysql_fetch_array(mysql_query("SELECT sum(qty) FROM tbl_transaction where product_id='$product_id' and status='F' and DATE_FORMAT(date_finish, '%Y-%m-%d') <=  '$sd' $add_query1"))or die (mysql_error());

                    $diff = $count_supply[0] - $count_trans[0];

                    if($diff>0){
                    ?>
                    <tr>
                        <th><?php echo $row['product_name'];?></th>
                        <th><?php echo $diff;?></th>
                    </tr>
                  <?php }}?>
               
                </tbody>
            </table>
        </div>                                            
    </div>                                        
</div>
 <br><br><br>
<div class="row justify-content-center">       
    <div class="col-lg-12 align-self-end">
        <div class="w-25 float-right">
            <small>Account Manager</small>
            <br><br><br>
            
            <p class="border-top"><?php echo ucwords($_SESSION['name']);?><br>Signature</p>
        </div>
    </div>
</div>
<hr>
</div>